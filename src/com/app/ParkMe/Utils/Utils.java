package com.app.ParkMe.Utils;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar;
import com.app.ParkMe.R;

public class Utils {

	public static void setCustomTitleActionBar(ActionBar actionBar, String title, int idLayout, Context ctx) {
		
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setBackgroundDrawable(ctx.getResources().getDrawable(R.drawable.bande));
		LayoutInflater inflate = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View v = inflate.inflate(idLayout, null);
		Typeface font = Typeface.createFromAsset(ctx.getAssets(), "fonts/chunkfive_ex.ttf");
		((TextView)v.findViewById(R.id.title_custom_actb)).setTypeface(font);
		((TextView)v.findViewById(R.id.title_custom_actb)).setText(title);
		actionBar.setCustomView(v);
	}
	
	public static void setTextCustomTitleActionBar(ActionBar actionBar, String title) {
		
		View v = actionBar.getCustomView();
		if (v != null) {
			((TextView)v.findViewById(R.id.title_custom_actb)).setText(title);
		}
	}
	
}
