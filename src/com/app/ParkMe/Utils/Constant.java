package com.app.ParkMe.Utils;

public interface Constant {

	// PREFERENCE KEY
	public static final String KEY_ACTIVE_CAMERA = "ActiveCamera";
	
	// FRAGMENT TAG
	public static final String NAME_FRAGMENT_MAP = "map";
	public static final String NAME_FRAGMENT_OPTION= "optionFragment";
	public static final String NAME_FRAGMENT_CAR = "carFragment";
	public static final String NAME_FRAGMENT_BUS = "busFragment";
	public static final String NAME_FRAGMENT_CAMPING = "campingFragment";
	public static final String NAME_FRAGMENT_PARK_DESC = "parkDescription";
	public static final String NAME_FRAGMENT_PARK_ROAD = "parkRoadList";
	
	// TYPE ELEMENT LIST
	public static final int TYPE_HEADER_SEARCH = 10;
	public static final int TYPE_HEADER_EXP_SEARCH_DEST = 20;
	public static final int TYPE_HEADER_EXP_CAT = 30;
	public static final int TYPE_HEADER_EXP_TOWN = 40;
	public static final int TYPE_HEADER_SEARCH_DEST = 50;
	public static final int TYPE_HEADER_SEARCH_CAT = 60;
	public static final int TYPE_HEADER_SEARCH_TOWN = 70;
	
	// HTTP CONSTANT
	public static final String URL_BASE_CAR = "http://dataprovence.cloudapp.net:8080/v1/dataprovencetourisme/ParkingsPublics/";
	public static final String URL_BASE_BUS = "http://dataprovence.cloudapp.net:8080/v1/dataprovencetourisme/ParkingsAutocars/";
	public static final String URL_BASE_CAMPING = "http://dataprovence.cloudapp.net:8080/v1/dataprovencetourisme/ParkingsCampingCars/";
	public static final String URL_BASE_ROAD = "http://maps.googleapis.com/maps/api/directions/json?";
	public static final String URL_BASE_DISTANCE = "http://maps.googleapis.com/maps/api/distancematrix/json?";
	
	public static final int REQUEST_TYPE_BASE = 1000;
	public static final int REQUEST_TYPE_ROAD = 1001;
	public static final int REQUEST_TYPE_DISTANCE = 1002;
	public static final int REQUEST_RESPONSE_NO_NETWORK = 5000;
	public static final int REQUEST_IO_EXCEPTION = 5001;
	
	public static final int BAD_ANSWER = 111;
	public static final int GOOD_ANSWER = 222;
	
	// XML VALUES
	public static final String PARTITION_KEY = "d:PartitionKey";
	public static final String ROW_KEY = "d:RowKey";
	public static final String TIMESTAMP = "d:Timestamp";
	public static final String ENTITY_ID = "d:entityid";
	public static final String RAISON_SOCIAL = "d:raisonsociale";
	public static final String TYPE = "d:type";
	public static final String SOUS_TYPE = "d:soustype";
	public static final String LIGNE_ADDR_1 = "d:ligneadresse1";
	public static final String NUMERO = "d:numro";
	public static final String COMPL_NUMERO = "d:complmentnumro";
	public static final String TYPE_DE_VOIE = "d:typedevoie";
	public static final String VOIE = "d:voie";
	public static final String LIGNE_ADDR_3 = "d:ligneadresse3";
	public static final String BOITE_POSTALE = "d:boitepostale";
	public static final String CODE_POSTAL = "d:codepostal";
	public static final String VILLE = "d:ville";
	public static final String BUREAU_DISTRIB = "d:bureaudistributeur";
	public static final String INDIC_CEDEX = "d:indicateurcdex";
	public static final String CDEX = "d:cdex";
	public static final String TELEPHONE = "d:tlphone";
	public static final String MAIL = "d:mail";
	public static final String WEB = "d:adresseweb";
	public static final String LONGITUDE = "d:longitude";
	public static final String LATITUDE = "d:latitude";
	
	// ID CURSOR LOADER
	public static final int ID_CURSOR_LOADER_PARK_TABLE = 1;
	public static final int ID_CURSOR_LOADER_PARK_TABLE_OPT = 2;
	public static final int ID_CURSOR_LOADER_TOWN = 3;
	public static final int ID_CURSOR_LOADER_ROAD = 4;
	
	// RESULT CODE ACTIVITY
	public static final int REQUEST_CODE_ACTIVITY_PARK = 10;
	public static final int REQUEST_CODE_ACTIVITY_MAP = 20;
	public static final int REQUEST_CODE_ACTIVITY_NAVIGATOR = 30;
	public static final int REQUEST_CODE_ACTIVITY_SETTINGS = 40;
	
	public static final int RESULT_CODE_CHANGE_CATEGORY = 100;
	
	// MAX ARROUND MARKER
	public static final int MAX_SIZE_ARRAY_MARKER = 3;
	
	// SEARCH ADRESS MAX RESULT
	public static final int MAX_RESULT_ADRESS = 10;
}
