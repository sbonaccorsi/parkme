package com.app.ParkMe.Utils;

import android.util.Log;

public class LogApp {

	private static final Boolean IS_DEBUG = false;
	
	public static void e(String tag, String msg) {
		if (IS_DEBUG)
			Log.e(tag, msg);
	}
}
