package com.app.ParkMe.Adapter;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.app.ParkMe.R;
import com.app.ParkMe.Data.ExpandableGroupe;
import com.app.ParkMe.Data.ExpandableObject;
import com.app.ParkMe.Data.PreferenceManager;
import com.app.ParkMe.Database.data.provider.HelpMeToParkMeContent.TOWN_TABLE;
import com.app.ParkMe.Listener.ChangeCategoryListener;
import com.app.ParkMe.Utils.Constant;

class ViewGroupeHolder {

	public TextView mTextHeader;
}

class ViewContentHolder {
	public TextView mTitleCheckBox;
	public CheckBox mCheckBox;
	public RadioButton mRadioButton;
	public LinearLayout mContainerCheck;
}

public class ExpandableListAdapter extends BaseExpandableListAdapter implements Constant {

	private Context mContext;
	private ArrayList<ExpandableGroupe> mArray;
	private LayoutInflater mInflater;
	private ChangeCategoryListener mListenerChange;

	public ExpandableListAdapter(Context context, ArrayList<ExpandableGroupe> array, ChangeCategoryListener listener) {
		mContext = context;
		mArray = array;
		mInflater = LayoutInflater.from(mContext);
		mListenerChange = listener;
	}

	public void setArrayGroup(ArrayList<ExpandableGroupe> array) {

		mArray = array;
		notifyDataSetChanged();
	}

	@Override
	public View getChildView(int groupPosition, final int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {

		final ExpandableObject object = (ExpandableObject) getChild(groupPosition, childPosition);
		final ViewContentHolder holder;
		if (convertView == null) {

			holder = new ViewContentHolder();
			convertView = mInflater.inflate(R.layout.row_content_expand_list, null);
			Typeface font = Typeface.createFromAsset(mContext.getAssets(), "fonts/chunkfive_ex.ttf");
			holder.mTitleCheckBox = (TextView) convertView.findViewById(R.id.title_checkbox_content);
			holder.mTitleCheckBox.setTypeface(font);
			holder.mContainerCheck = (LinearLayout) convertView.findViewById(R.id.container_contentSearchOptionCheckBox);
			holder.mCheckBox = (CheckBox) convertView.findViewById(R.id.checkBox_content_expand_list);
			holder.mRadioButton = (RadioButton) convertView.findViewById(R.id.radioButton_content_expand_list);
			convertView.setTag(holder);
		}
		else
			holder = (ViewContentHolder) convertView.getTag();

		if (object.getmGroupe().getType() == TYPE_HEADER_EXP_CAT) {

			int type = PreferenceManager.getInstance(mContext).getInt("type");
			holder.mRadioButton.setTag(childPosition);
			if (type == childPosition)
				holder.mRadioButton.setChecked(true);
			else
				holder.mRadioButton.setChecked(false);
			holder.mContainerCheck.setVisibility(View.VISIBLE);
			holder.mRadioButton.setVisibility(View.VISIBLE);
			holder.mCheckBox.setVisibility(View.GONE);
			holder.mTitleCheckBox.setText(object.getmName());
			holder.mTitleCheckBox.setCompoundDrawablesWithIntrinsicBounds(mArray.get(groupPosition)
					.getmArray().get(childPosition).getRessource(), 0, 0, 0);
			holder.mRadioButton.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					if (((Integer)buttonView.getTag()).intValue() != 
							PreferenceManager.getInstance(mContext).getInt("type") && 
							isChecked)
					mListenerChange.changeCategory(childPosition);
				}
			});

		}
		else if (object.getmGroupe().getType() == TYPE_HEADER_EXP_TOWN) {

			holder.mTitleCheckBox.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);

			holder.mContainerCheck.setVisibility(View.VISIBLE);
			holder.mRadioButton.setVisibility(View.GONE);
			holder.mCheckBox.setVisibility(View.VISIBLE);
			holder.mTitleCheckBox.setText(object.getmName());
			holder.mCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					
					if (isChecked) {
						ContentValues values = new ContentValues();
						values.put(TOWN_TABLE.Columns.NAME_TOWN.getName(), holder.mTitleCheckBox.getText().toString());
						mContext.getContentResolver().insert(TOWN_TABLE.CONTENT_URI, values);
					}
					else {
						mContext.getContentResolver().delete(TOWN_TABLE.CONTENT_URI, TOWN_TABLE.Columns.NAME_TOWN.getName() + 
								"=?", new String[]{holder.mTitleCheckBox.getText().toString()});
					}
				}
			});
			Cursor cursor = mContext.getContentResolver().query(TOWN_TABLE.CONTENT_URI, TOWN_TABLE.PROJECTION, 
					TOWN_TABLE.Columns.NAME_TOWN.getName() + "=?", new String[]{holder.mTitleCheckBox.getText().toString()}, null);
			if (cursor.moveToFirst()) {
				String town = cursor.getString(cursor.getColumnIndex(TOWN_TABLE.Columns.NAME_TOWN.getName()));
				if (town.contentEquals(holder.mTitleCheckBox.getText().toString())) {
					holder.mCheckBox.setChecked(true);
				}
				else {
					holder.mCheckBox.setChecked(false);
				}
			}
			else {
				holder.mCheckBox.setChecked(false);
			}
			cursor.close();
		}
		return convertView;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {

		ExpandableGroupe groupe = (ExpandableGroupe) getGroup(groupPosition);
		final ViewGroupeHolder holder;
		if (convertView == null) {

			holder = new ViewGroupeHolder();
			convertView = mInflater.inflate(R.layout.row_header_expand_list, null);
			Typeface font = Typeface.createFromAsset(mContext.getAssets(), "fonts/chunkfive_ex.ttf");
			holder.mTextHeader = (TextView) convertView.findViewById(R.id.title_header_expand_list);
			holder.mTextHeader.setTypeface(font);
			convertView.setTag(holder);
		}
		else
			holder = (ViewGroupeHolder) convertView.getTag();

		//		if (groupe.getType() == TYPE_HEADER_SEARCH) {
		//			holder.mTextHeader.setCompoundDrawablesWithIntrinsicBounds(mContext.getResources().getDrawable(R.drawable.abs__ic_search_api_holo_light),
		//					null, null, null);
		//		}
		//		else if (groupe.getType() == TYPE_HEADER_EXP_SEARCH_DEST || groupe.getType() == TYPE_HEADER_EXP_CAT ||
		//				groupe.getType() == TYPE_HEADER_EXP_TOWN)
		//		holder.mTextHeader.setCompoundDrawablesWithIntrinsicBounds(mContext.getResources().getDrawable(R.drawable.abs__ic_go_search_api_holo_light),
		//				null, null, null);

		holder.mTextHeader.setText(groupe.getmName());

		return convertView;
	}

	@Override
	public boolean areAllItemsEnabled() {
		return true;
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return mArray.get(groupPosition).getmArray().get(childPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return mArray.get(groupPosition).getmArray().size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return mArray.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return mArray.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}
}
