package com.app.ParkMe.Adapter;

import android.content.Context;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.graphics.Typeface;
import android.support.v4.widget.SimpleCursorAdapter;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.ParkMe.R;
import com.app.ParkMe.Database.data.provider.HelpMeToParkMeContent.ROAD_TABLE;
import com.app.ParkMe.Utils.Constant;

class ViewHolder {

	public TextView mInst;
	public TextView mDist;
	public TextView mTime;
	public LinearLayout mContainer;
}

public class ListRoadAdapter extends SimpleCursorAdapter implements Constant {

	private Context mCtx;

	public ListRoadAdapter(Context context, Cursor c, AssetManager asset) {

		super(context, R.layout.row_list_road_park, c, new String[] {}, new int[] {}, FLAG_REGISTER_CONTENT_OBSERVER);
		mCtx = context;
	}
	
	@Override
	public Cursor getCursor() {
		return super.getCursor();
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		View v = super.getView(position, convertView, parent);
		Cursor cursor = getCursor();
		cursor.moveToPosition(position);
		
		ViewHolder holder = (ViewHolder) v.getTag();
		if (holder == null) {
			
			holder = new ViewHolder();
			holder.mContainer = (LinearLayout) v.findViewById(R.id.container_row_road);
			Typeface fontM = Typeface.createFromAsset(mContext.getAssets(), "fonts/chunkfive_ex.ttf");
			Typeface fontS = Typeface.createFromAsset(mContext.getAssets(), "fonts/chunkfive_ex.ttf");
			holder.mInst = (TextView) v.findViewById(R.id.intruction_title);
			holder.mInst.setTypeface(fontM);
			holder.mDist = (TextView) v.findViewById(R.id.distance_title);
			holder.mDist.setTypeface(fontS);
			holder.mTime = (TextView) v.findViewById(R.id.duration_title);
			holder.mTime.setTypeface(fontS);
			v.setTag(holder);
		}
		holder.mInst.setText(Html.fromHtml(cursor.getString(cursor.getColumnIndex(ROAD_TABLE.Columns.INSTRUCTION.getName()))).toString());
		
		holder.mTime.setText(". " + Html.fromHtml(cursor.getString(cursor.getColumnIndex(ROAD_TABLE.Columns.DURATION.getName()))).toString());
		holder.mDist.setText(". " + Html.fromHtml(cursor.getString(cursor.getColumnIndex(ROAD_TABLE.Columns.DISTANCE.getName()))).toString());
		
		Animation anim = AnimationUtils.loadAnimation(mCtx, R.anim.translate_row_road);
		holder.mInst.startAnimation(anim);
		holder.mContainer.startAnimation(anim);
		
		return v;
	}
}

