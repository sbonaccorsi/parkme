package com.app.ParkMe.Adapter;

import java.util.List;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.app.ParkMe.R;
import com.app.ParkMe.Utils.Constant;

public class VehicleViewPagerAdapter extends FragmentPagerAdapter implements Constant {

	private List<Fragment> mList;
	private Context mContext;
	
	private String[] TITLE_PAGER;
	
	public VehicleViewPagerAdapter(FragmentManager fm, List<Fragment> list, Context ctx) {
		super(fm);
		mList = list;
		mContext = ctx;
		String[] tab = {mContext.getResources().getString(R.string.string_name_cat_car),
				mContext.getResources().getString(R.string.string_name_cat_bus),
				mContext.getResources().getString(R.string.string_name_cat_campeur)};
		TITLE_PAGER = tab;
	}

	@Override
	public Fragment getItem(int idx) {
		return mList.get(idx);
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return TITLE_PAGER[position % TITLE_PAGER.length];
	}
	
	@Override
	public int getCount() {
		return mList.size();
	}

}
