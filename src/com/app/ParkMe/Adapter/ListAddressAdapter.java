package com.app.ParkMe.Adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

import com.app.ParkMe.R;
import com.app.ParkMe.Map.AddressData;

class ViewHolderAddress {

	public CheckBox mCheck;
	public TextView mText;
}

public class ListAddressAdapter extends BaseAdapter {

	private ArrayList<AddressData> mList;
	private ArrayList<AddressData> mSelected = new ArrayList<AddressData>();
	private Context mContext;
	private LayoutInflater mInflater;

	public ListAddressAdapter(Context ctx, ArrayList<AddressData> list) {

		mList = list;
		mContext = ctx;
		mInflater = LayoutInflater.from(mContext);
	}

	@Override
	public int getCount() {
		return mList.size();
	}

	@Override
	public Object getItem(int position) {
		return mList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	public ArrayList<AddressData> getSelectedElement() {
		return mSelected;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		final int pos = position;
		ViewHolderAddress holder = null;
		if (convertView == null) {
			holder = new ViewHolderAddress();
			convertView = mInflater.inflate(R.layout.row_list_address, null);
			holder.mCheck = (CheckBox) convertView.findViewById(R.id.checkBox_address);
			holder.mText = (TextView) convertView.findViewById(R.id.textView_address);
			convertView.setTag(holder);

		} else {
			holder = (ViewHolderAddress)convertView.getTag();
		}

		holder.mCheck.setChecked(mList.get(position).ismSelected());
		holder.mCheck.setText(mList.get(position).getmAddr().getCountryName());

		holder.mCheck.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

				mList.get(pos).setmSelected(isChecked);
				if (isChecked)
					mSelected.add(mList.get(pos));
				else
					mSelected.remove(mList.get(pos));
			}
		});


		holder.mText.setText("");
		for (int idx = 0; idx < mList.get(position).getmAddr().getMaxAddressLineIndex(); ++idx)
			holder.mText.setText(holder.mText.getText().toString() + mList.get(position).getmAddr().getAddressLine(idx) + " ");
		return convertView;
	}

}
