package com.app.ParkMe.Listener;

import android.content.DialogInterface;

public interface OnClickMessageBoxButton {

	void OnClickPositiveButton(DialogInterface dialog, int which);
	void OnClickNegativeButton(DialogInterface dialog, int which);
	void OnClickNeutralButton(DialogInterface dialog, int which);
}
