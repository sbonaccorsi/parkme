package com.app.ParkMe.Listener;

public interface SearchDestinationListener {

	public void onSearchDestination(String dest, int group);
}
