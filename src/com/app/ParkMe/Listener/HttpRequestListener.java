package com.app.ParkMe.Listener;

import com.app.ParkMe.Network.AnswerData;

public interface HttpRequestListener {

	public void onRequestIsComplete(AnswerData answer);
}
