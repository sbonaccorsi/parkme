package com.app.ParkMe.Fragment;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.app.Dialog;
import android.database.Cursor;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import com.app.ParkMe.R;
import com.app.ParkMe.Activity.BaseGuiActivity;
import com.app.ParkMe.Adapter.ListAddressAdapter;
import com.app.ParkMe.Algorithm.ComparatorDist;
import com.app.ParkMe.Data.PreferenceManager;
import com.app.ParkMe.Database.data.provider.HelpMeToParkMeContent.PARK_TABLE;
import com.app.ParkMe.Database.data.provider.HelpMeToParkMeContent.TOWN_TABLE;
import com.app.ParkMe.Listener.HttpRequestListener;
import com.app.ParkMe.Map.AddressData;
import com.app.ParkMe.Map.CustomMarker;
import com.app.ParkMe.Map.DistanceProvider;
import com.app.ParkMe.Map.MarkerData;
import com.app.ParkMe.Map.Road;
import com.app.ParkMe.Map.RoadProvider;
import com.app.ParkMe.Network.AnswerData;
import com.app.ParkMe.Network.TaskManager;
import com.app.ParkMe.Network.TaskQueue;
import com.app.ParkMe.Utils.Constant;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.CancelableCallback;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

public class MapGoogleFragment extends SupportMapFragment implements Constant, LoaderCallbacks<Cursor>,
OnInfoWindowClickListener, OnCameraChangeListener, OnMarkerClickListener, OnMapClickListener, CancelableCallback, HttpRequestListener {

	private GoogleMap mMap;
	private Polygon mMutablePolygon;
	private Geocoder mGeocoder;
	private ArrayList<MarkerData> mArrayMarker;
	private ArrayList<Polyline> mArrayPolyline;
	private ArrayList<MarkerData> mArrayMarkerDest;
	private Location mLoc = new Location("p1");
	private TaskQueue mQeue;
	private PreferenceManager mPref;
	private CustomMarker mCustomMarker;

	private int mIndexArrayDest;

	public MapGoogleFragment() {

	}

	public static MapGoogleFragment newInstance() {

		MapGoogleFragment mp = new MapGoogleFragment();
		return mp;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		mMap = this.getMap();
		if (mMap != null) {

			mPref = PreferenceManager.getInstance(getActivity());

			mArrayPolyline = new ArrayList<Polyline>();
			mArrayMarker = new ArrayList<MarkerData>();
			mArrayMarkerDest = new ArrayList<MarkerData>();

			mQeue = TaskQueue.getInstance();

			mMap.setMyLocationEnabled(true);
			mMap.getUiSettings().setMyLocationButtonEnabled(true);

			mMap.setOnInfoWindowClickListener(this);
			mMap.setOnCameraChangeListener(this);
			mMap.setOnMarkerClickListener(this);
			mMap.setOnMapClickListener(this);

			mCustomMarker = new CustomMarker(getActivity(), this);
			mMap.setInfoWindowAdapter(mCustomMarker);

			mGeocoder = new Geocoder(getActivity(), getActivity().getResources().getConfiguration().locale);

			getActivity().getSupportLoaderManager().initLoader(ID_CURSOR_LOADER_PARK_TABLE, null, this);
			getActivity().getSupportLoaderManager().initLoader(ID_CURSOR_LOADER_TOWN, null, this);
		}
		else
			Crouton.makeText(getActivity(), getString(R.string.string_error_on_load_map), Style.ALERT).show();
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}

	public ArrayList<MarkerData> getArrayDestination() {
		return mArrayMarkerDest;
	}

	public void centerMapOnUser() {
		if (mMap != null) {
			this.moveCameraMap(mMap.getMyLocation());
		}
	}

	public void centerMapOnLocation(Location location) {

		if (mMap != null) {
			this.moveCameraMap(location);
		}
	}

	private void moveCameraMap(Location location) {
		if (location != null) {
			CameraPosition anim = new CameraPosition.Builder()
			.target(new LatLng(location.getLatitude(), location.getLongitude()))
			.zoom(15).build();
			mMap.animateCamera(CameraUpdateFactory.newCameraPosition(anim), null);
		}
	}

	private void addMarkerOnMap(Cursor cursor) {

		double lon = Double.parseDouble(cursor.getString(cursor.getColumnIndex(PARK_TABLE.Columns.LONGITUDE.getName())));
		double lat = Double.parseDouble(cursor.getString(cursor.getColumnIndex(PARK_TABLE.Columns.LATITUDE.getName())));
		String id = cursor.getString(cursor.getColumnIndex(PARK_TABLE.Columns.ENTITY_ID.getName()));
		LatLng coord = new LatLng(lat, lon);
		String title = cursor.getString(cursor.getColumnIndex(PARK_TABLE.Columns.RAISON_SOCIAL.getName()));

		Marker m = mMap.addMarker(new MarkerOptions().position(coord).title(title)
				.icon(BitmapDescriptorFactory.fromResource(mPref.getInt("flagValue"))));

		MarkerData data = new MarkerData();
		data.setmTown(cursor.getString(cursor.getColumnIndex(PARK_TABLE.Columns.VILLE.getName())));
		data.setmMarker(m);
		data.setmEntityId(id);
		mLoc.setLatitude(lat);
		mLoc.setLongitude(lon);
		mArrayMarker.add(data);		
	}

	public void startRequestDistance(MarkerData data) {

		if (mArrayMarker != null && mArrayMarker.size() > 0) {

			if (mMap != null && mMap.getMyLocation() != null) {

				Location dest = null;
				if (data == null) {
					dest = mMap.getMyLocation();
				}
				else {
					dest = new Location("p2");
					dest.setLatitude(data.getMarkerDestination().getPosition().latitude);
					dest.setLongitude(data.getMarkerDestination().getPosition().longitude);
				}
				for (int idx = 0; idx < mArrayMarker.size(); ++idx) {
					mLoc.setLatitude(mArrayMarker.get(idx).getmMarker().getPosition().latitude);
					mLoc.setLongitude(mArrayMarker.get(idx).getmMarker().getPosition().longitude);
					mArrayMarker.get(idx).setmDist(dest.distanceTo(mLoc));
				}
				Collections.sort(mArrayMarker, new ComparatorDist());
				if (data != null) {
					data.setmMarker(mArrayMarker.get(0).getmMarker());
					mArrayMarkerDest.add(data);
				}

				mLoc.set(dest);
				String url = this.createRequestDistance(dest);

				TaskManager task = new TaskManager(this, getActivity(), url, REQUEST_TYPE_DISTANCE, null);
				task.setData1(Double.valueOf(mArrayMarker.get(0).getmMarker().getPosition().latitude));
				task.setData2(Double.valueOf(mArrayMarker.get(0).getmMarker().getPosition().longitude));
				task.setData3(Double.valueOf(mLoc.getLatitude()));
				task.setData4(Double.valueOf(mLoc.getLongitude()));
				task.setParam(mArrayMarker);

				((BaseGuiActivity)getActivity()).manageProgressBar(true);
				mQeue.addTaskInQueue(task);
			}
			else
				Crouton.makeText(getActivity(), getString(R.string.string_error_locate_usr), Style.ALERT).show();
		}
		else
			Crouton.makeText(getActivity(), getString(R.string.string_error_get_data), Style.ALERT).show();
	}

	private String createRequestDistance(Location loc) {

		String urlBase = DistanceProvider.getBaseUrl_StartPosition(loc.getLatitude(), loc.getLongitude());
		urlBase = DistanceProvider.putDestinationParam(urlBase);

		int size = 0;
		if (mArrayMarker.size() > 10)
			size = 10;
		else
			size = mArrayMarker.size();
		for (int idx = 0; idx < size; ++idx) {

			urlBase = DistanceProvider.makeUrlWithDestination(urlBase, mArrayMarker.get(idx).getmMarker().getPosition().latitude, 
					mArrayMarker.get(idx).getmMarker().getPosition().longitude);
			if (idx != size - 1)
				try {
					urlBase = DistanceProvider.putSeparator(urlBase, URLEncoder.encode("|", "UTF-8"));
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
		}
		urlBase = DistanceProvider.putParameterSensor(urlBase, "false");
		return urlBase;
	}

	public void searchDestination(String dest) {
		AsyncGeocoder task = new AsyncGeocoder();
		task.execute(dest);
	}

	private void displayDialogAddr(ArrayList<AddressData> list) {

		final Dialog dial = new Dialog(getActivity());
		dial.setContentView(R.layout.view_dialog_address);
		dial.setTitle(getString(R.string.string_title_dialog));
		ListView lv = (ListView) dial.findViewById(R.id.listView_address_dialog);
		Button cancel = (Button) dial.findViewById(R.id.button_cancel_dial);
		cancel.setText(getString(R.string.string_cancel));
		Button ok = (Button) dial.findViewById(R.id.button_ok_dial);
		ok.setText(getString(R.string.string_validate));
		final ListAddressAdapter adapter = new ListAddressAdapter(getActivity(), list);
		lv.setAdapter(adapter);
		dial.show();
		cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dial.dismiss();
			}
		});
		ok.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				clearMap();

				ArrayList<AddressData> l = adapter.getSelectedElement();
				MapGoogleFragment.this.displayDestinationOnMap(l);

				dial.dismiss();
			}
		});
	}

	private void displayDestinationOnMap(ArrayList<AddressData> list) {

		if (mMap != null && mMap.getMyLocation() != null) {
			for (int idx = 0; idx < list.size(); ++idx) {

				String addr = "";
				for (int i = 0; i < list.get(idx).getmAddr().getMaxAddressLineIndex(); ++i) {
					addr += list.get(idx).getmAddr().getAddressLine(i) + " ";
				}

				MarkerData data = new MarkerData();
				Marker m = mMap.addMarker(new MarkerOptions().title(addr).position(new LatLng(list.get(idx).getmAddr().getLatitude(), 
						list.get(idx).getmAddr().getLongitude()))
						.icon(BitmapDescriptorFactory.fromResource(R.drawable.map_pin_holed_blue_normal)));
				data.setMarkerDestination(m);

				this.startRequestDistance(data);
			}
			this.displayArrowNavigation(list);
			if (list.isEmpty() == false) {
				Location location = new Location(LocationManager.NETWORK_PROVIDER);
				location.setLatitude(list.get(0).getmAddr().getLatitude());
				location.setLongitude(list.get(0).getmAddr().getLongitude());
				this.centerMapOnLocation(location);
			}
		}
		else 
			Crouton.makeText(getActivity(), getString(R.string.string_error_locate_usr), Style.ALERT).show();
	}

	private void displayArrowNavigation(ArrayList<AddressData> list) {

		if (list.size() > 1) {
			((BaseGuiActivity)getActivity()).displayContainerNav(View.VISIBLE);
		}
	}

	public boolean isInSearchDestinationMode() {
		if (mArrayMarkerDest != null && mArrayMarkerDest.size() > 1)
			return true;
		else 
			return false;
	}

	public void nextDestination() {

		if (mArrayMarkerDest.isEmpty() == false) {
			mIndexArrayDest++;
			if (mIndexArrayDest == mArrayMarkerDest.size())
				mIndexArrayDest = 0;
			Location loc = new Location(LocationManager.NETWORK_PROVIDER);
			loc.setLatitude(mArrayMarkerDest.get(mIndexArrayDest).getmMarker().getPosition().latitude);
			loc.setLongitude(mArrayMarkerDest.get(mIndexArrayDest).getmMarker().getPosition().longitude);
			this.centerMapOnLocation(loc);
		}
	}

	public void prevDestination() {

		if (mArrayMarkerDest.isEmpty() == false) {
			mIndexArrayDest--;
			if (mIndexArrayDest < 0)
				mIndexArrayDest = mArrayMarkerDest.size() - 1;
			Location loc = new Location(LocationManager.NETWORK_PROVIDER);
			loc.setLatitude(mArrayMarkerDest.get(mIndexArrayDest).getmMarker().getPosition().latitude);
			loc.setLongitude(mArrayMarkerDest.get(mIndexArrayDest).getmMarker().getPosition().longitude);
			this.centerMapOnLocation(loc);
		}
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle param) {

		switch (id) {
		case ID_CURSOR_LOADER_PARK_TABLE :
			return new CursorLoader(getActivity(), PARK_TABLE.CONTENT_URI, PARK_TABLE.PROJECTION, null, null, null);
		case ID_CURSOR_LOADER_TOWN :
			return new CursorLoader(getActivity(), TOWN_TABLE.CONTENT_URI, TOWN_TABLE.PROJECTION, null, null, null);
		}
		return null;
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {

		switch (loader.getId()) {
		case ID_CURSOR_LOADER_PARK_TABLE:

			if (cursor.moveToFirst()) {
				do {
					if (mArrayMarker.size() == 0)
						this.addMarkerOnMap(cursor);
					boolean ret = false;
					for (int idx = 0; idx < mArrayMarker.size(); ++idx) {
						String id = mArrayMarker.get(idx).getmEntityId();
						if (id.contentEquals(cursor.getString(cursor.getColumnIndex(PARK_TABLE.Columns.ENTITY_ID.getName()))))
							ret = true;
					}
					if (!ret)
						this.addMarkerOnMap(cursor);

				} while (cursor.moveToNext());
			}
			break;

		case ID_CURSOR_LOADER_TOWN :

			this.filterTownDisplayOnMap(cursor);
			break;
		}
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
	}

	private void filterTownDisplayOnMap(Cursor cursor) {

		for (int idx = 0; idx < mArrayMarker.size(); ++idx)
			mArrayMarker.get(idx).getmMarker().setVisible(false);
		if (cursor.moveToFirst()) {

			do {
				String town = cursor.getString(cursor.getColumnIndex(TOWN_TABLE.Columns.NAME_TOWN.getName()));
				for (int idx = 0; idx < mArrayMarker.size(); ++idx) {
					if (town.contentEquals(mArrayMarker.get(idx).getmTown()))
						mArrayMarker.get(idx).getmMarker().setVisible(true);
				}
			} while (cursor.moveToNext());
		}
		else {
			for (int idx = 0; idx < mArrayMarker.size(); ++idx)
				mArrayMarker.get(idx).getmMarker().setVisible(true);
		}
	}

	@Override
	public void onInfoWindowClick(Marker marker) {

		boolean isClickMarkerDrawWithDest = false;
		boolean isDestMarkerClick = false;
		double lat = -1;
		double lon = -1;
		MarkerData data = null;

		for (int idx = 0; idx < mArrayMarkerDest.size(); ++idx) {
			if (marker.getPosition().latitude == mArrayMarkerDest.get(idx).getmMarker().getPosition().latitude && 
					marker.getPosition().longitude == mArrayMarkerDest.get(idx).getmMarker().getPosition().longitude) {
				isClickMarkerDrawWithDest = true;
				data = mArrayMarkerDest.get(idx);
			}
			if (marker.getPosition().latitude == mArrayMarkerDest.get(idx).getMarkerDestination().getPosition().latitude && 
					marker.getPosition().longitude == mArrayMarkerDest.get(idx).getMarkerDestination().getPosition().longitude)
				isDestMarkerClick = true;

		}
		if (!isClickMarkerDrawWithDest && !isDestMarkerClick) {

			if (mMap != null && mMap.getMyLocation() != null) {
				lat = mMap.getMyLocation().getLatitude();
				lon = mMap.getMyLocation().getLongitude();
			}
			((BaseGuiActivity)getActivity()).launchIntentParkActivity(marker.getTitle(), 
					marker.getPosition().latitude, marker.getPosition().longitude, lat, lon);
		}
		else {
			if (isClickMarkerDrawWithDest && !isDestMarkerClick) {
				lat = data.getMarkerDestination().getPosition().latitude;
				lon = data.getMarkerDestination().getPosition().longitude;
				((BaseGuiActivity)getActivity()).launchIntentParkActivity(marker.getTitle(), 
						marker.getPosition().latitude, marker.getPosition().longitude, lat, lon);
			}
		}
	}

	@Override
	public void onCameraChange(CameraPosition position) {

	}

	@Override
	public boolean onMarkerClick(Marker marker) {

		CameraPosition anim = new CameraPosition.Builder()
		.target(marker.getPosition()).zoom(mMap.getCameraPosition().zoom)
		.bearing(mMap.getCameraPosition().bearing)
		.tilt(mMap.getCameraPosition().tilt).build();
		mMap.animateCamera(CameraUpdateFactory.newCameraPosition(anim), null);

		marker.showInfoWindow();

		if (PreferenceManager.getInstance(getActivity()).getBoolean(KEY_ACTIVE_CAMERA)) {
			if (mMutablePolygon != null)
				mMutablePolygon.remove();
			this.createPolygoneOnMarker(marker);
			this.createAnimateCameraOnMarker(marker);
		}
		return true;
	}

	private void createAnimateCameraOnMarker(Marker marker) {

		CameraPosition anim = new CameraPosition.Builder()
		.target(marker.getPosition())
		.zoom(18.5f)
		.bearing(300)
		.tilt(50)
		.build();
		mMap.animateCamera(CameraUpdateFactory.newCameraPosition(anim), null);
	}

	private void createPolygoneOnMarker(Marker marker) {

		PolygonOptions options = new PolygonOptions();
		int numPoints = 400;
		float semiHorizontalAxis = 0.001f;
		float semiVerticalAxis = 0.0005f;
		double phase = 2 * Math.PI / numPoints;
		for (int i = 0; i <= numPoints; i++) {
			options.add(new LatLng(marker.getPosition().latitude + semiVerticalAxis * Math.sin(i * phase),
					marker.getPosition().longitude + semiHorizontalAxis * Math.cos(i * phase)));
		}

		int fillColor = Color.HSVToColor(
				100, new float[] {200, 1, 1});
		mMutablePolygon = mMap.addPolygon(options
				.strokeWidth(5)
				.strokeColor(Color.BLACK)
				.fillColor(fillColor));
	}

	@Override
	public void onMapClick(LatLng point) {

	}

	public boolean clearMap() {

		boolean ret = false;
		if (mMutablePolygon != null) {
			mMutablePolygon.remove();
			mMutablePolygon = null;
			ret = true;
		}

		if (mArrayPolyline != null) {
			for (int idx = 0; idx < mArrayPolyline.size(); ++idx) {
				mArrayPolyline.get(idx).remove();
				ret = true;
			}
			mArrayPolyline.clear();
		}
		if (mArrayMarkerDest != null) {
			for (int idx = 0; idx < mArrayMarkerDest.size(); ++idx) {
				mArrayMarkerDest.get(idx).getMarkerDestination().remove();
				ret = true;
			}
			mArrayMarkerDest.clear();
		}
		mIndexArrayDest = 0;
		((BaseGuiActivity)getActivity()).displayContainerNav(View.GONE);
		return ret;
	}

	@Override
	public void onCancel() {
	}

	@Override
	public void onFinish() {

	}

	private void drawPath(Road road) {

		for (int idx = 0; idx < road.getArrayCoord().size(); ++idx) {

			LatLng c1 = new LatLng(road.getArrayCoord().get(idx).getmLatStart(), road.getArrayCoord().get(idx).getmLonStart());
			LatLng c2 = new LatLng(road.getArrayCoord().get(idx).getmLatEnd(), road.getArrayCoord().get(idx).getmLonEnd());

			mArrayPolyline.add(mMap.addPolyline((new PolylineOptions()).
					add(c1, c2).geodesic(true).color(Color.BLACK)));
		}
	}

	private void triMarkerDistance() {

		Collections.sort(mArrayMarker, new ComparatorDist());
	}

	@Override
	public void onRequestIsComplete(final AnswerData answer) {

		switch (answer.getTypeRequest()) {
		case REQUEST_TYPE_ROAD:
			if (answer.getStatusRequest() == GOOD_ANSWER) {
				final Road road = (Road) answer.getData();
				drawPath(road);
			}
			else if (answer.getStatusRequest() == BAD_ANSWER) {
				if (answer.getStatusCode() == REQUEST_RESPONSE_NO_NETWORK)
					Crouton.makeText(getActivity(), getString(R.string.string_error_connection), Style.ALERT).show();
				else if (answer.getStatusCode() == REQUEST_IO_EXCEPTION) {
					Crouton.makeText(getActivity(), getString(R.string.string_server_no_respond), Style.ALERT).show();
				}
			}
			((BaseGuiActivity)getActivity()).setSupportProgressBarIndeterminateVisibility(false);
			break;

		case REQUEST_TYPE_DISTANCE:

			if (answer.getStatusRequest() == GOOD_ANSWER) {
				final AnswerData ans = answer;
				MapGoogleFragment.this.triMarkerDistance();
				String url = RoadProvider.getUrlRoadMap(((Double)ans.getTask().getData3()).doubleValue(), 
						((Double)ans.getTask().getData4()).doubleValue(), 
						((Double)ans.getTask().getData1()).doubleValue(), 
						((Double)ans.getTask().getData2()).doubleValue());
				TaskManager task = new TaskManager(MapGoogleFragment.this, getActivity(), url, REQUEST_TYPE_ROAD, null);
				mQeue.addTaskInQueue(task);
			}
			else if (answer.getStatusRequest() == BAD_ANSWER) {
				((BaseGuiActivity)getActivity()).setSupportProgressBarIndeterminateVisibility(false);
				if (answer.getStatusCode() == REQUEST_RESPONSE_NO_NETWORK) {
					Crouton.makeText(getActivity(), getString(R.string.string_error_connection), Style.ALERT).show();
				}
				else if (answer.getStatusCode() == REQUEST_IO_EXCEPTION) {
					Crouton.makeText(getActivity(), getString(R.string.string_server_no_respond), Style.ALERT).show();
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		if (mMap != null) {
			mMap.setOnInfoWindowClickListener(null);
			mMap.setOnCameraChangeListener(null);
			mMap.setOnMarkerClickListener(null);
			mMap.setOnMapClickListener(null);
			for (int idx = 0; idx < mArrayMarker.size(); ++idx)
				mArrayMarker.remove(idx);
			for (int idx = 0; idx < mArrayMarkerDest.size(); ++idx)
				mArrayMarkerDest.remove(idx);
			for (int idx = 0; idx < mArrayPolyline.size(); ++idx)
				mArrayPolyline.remove(idx);
		}

		mLoc = null;
		mCustomMarker = null;
		mMap = null;
		mMutablePolygon = null;
		mGeocoder = null;
	}

	private class AsyncGeocoder extends AsyncTask<String, Void, ArrayList<AddressData>> {

		@Override
		protected ArrayList<AddressData> doInBackground(String... params) {

			List<Address> addrs = null;
			ArrayList<AddressData> list = null;
			try {
				addrs = mGeocoder.getFromLocationName(params[0], MAX_RESULT_ADRESS);
				list = new ArrayList<AddressData>();
				for (int idx = 0; idx < addrs.size(); ++idx) {
					list.add(new AddressData(addrs.get(idx)));
					list.get(idx).setmSelected(false);
				}
			} catch (IOException e) {
				e.printStackTrace();
				Crouton.makeText(getActivity(), getString(R.string.string_error_connection), Style.ALERT).show();
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
			return list;
		}

		@Override
		protected void onPostExecute(ArrayList<AddressData> result) {
			super.onPostExecute(result);
			if (result.size() > 1) {
				displayDialogAddr(result);
			}
			else if (result.size() == 1)
				displayDestinationOnMap(result);
			else if (result.size() == 0)
				Crouton.makeText(getActivity(), getString(R.string.string_no_result_search_dest), Style.ALERT).show();
		}
	}
}
