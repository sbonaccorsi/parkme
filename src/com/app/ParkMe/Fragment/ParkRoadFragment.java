package com.app.ParkMe.Fragment;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.actionbarsherlock.app.SherlockListFragment;
import com.app.ParkMe.R;
import com.app.ParkMe.Adapter.ListRoadAdapter;
import com.app.ParkMe.Database.data.provider.HelpMeToParkMeContent.ROAD_TABLE;
import com.app.ParkMe.Utils.Constant;

public class ParkRoadFragment extends SherlockListFragment implements Constant, LoaderCallbacks<Cursor> {

	private ListRoadAdapter mAdapter;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_road_park, container, false);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		mAdapter = new ListRoadAdapter(getActivity(), null, getActivity().getAssets());
		getListView().setAdapter(mAdapter);
		getActivity().getSupportLoaderManager().initLoader(ID_CURSOR_LOADER_ROAD, null, this);
	}
	
	public void onResume() {
		super.onResume();
		
		
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle param) {

		switch (id) {
		case ID_CURSOR_LOADER_ROAD:
			return new CursorLoader(getActivity(), ROAD_TABLE.CONTENT_URI, ROAD_TABLE.PROJECTION, null, null, null);
		}
		
		return null;
	}

	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
		
		switch (loader.getId()) {
		case ID_CURSOR_LOADER_ROAD:
			mAdapter.swapCursor(cursor);
			break;
		}
	}

	public void onLoaderReset(Loader<Cursor> loader) {

		mAdapter.swapCursor(null);
	}
}
