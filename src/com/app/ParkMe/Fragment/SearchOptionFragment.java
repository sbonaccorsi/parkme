package com.app.ParkMe.Fragment;

import java.util.ArrayList;

import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.app.ParkMe.R;
import com.app.ParkMe.Activity.BaseGuiActivity;
import com.app.ParkMe.Adapter.ExpandableListAdapter;
import com.app.ParkMe.Data.ExpandableGroupe;
import com.app.ParkMe.Data.ExpandableObject;
import com.app.ParkMe.Database.data.provider.HelpMeToParkMeContent.PARK_TABLE;
import com.app.ParkMe.Listener.SearchDestinationListener;
import com.app.ParkMe.Utils.Constant;
import com.app.ParkMe.Utils.LogApp;

public class SearchOptionFragment extends SherlockFragment implements Constant, OnGroupClickListener, LoaderCallbacks<Cursor>, OnChildClickListener,
SearchDestinationListener {

	private TextView mTitleSearchOption;
	private ExpandableListAdapter mAdapter;
	private ExpandableListView mExpandable;
	private ArrayList<ExpandableGroupe> mArrayGroupe = new ArrayList<ExpandableGroupe>();
	private ExpandableGroupe mTown;
	private ArrayList<ExpandableObject> mArrayTown = new ArrayList<ExpandableObject>();
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_search_option, container, false);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		mTitleSearchOption = (TextView) view.findViewById(R.id.title_cell_searchOption);
		mTitleSearchOption.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/chunkfive_ex.ttf"));
		
		mTown = new ExpandableGroupe(getActivity().getResources().getString(R.string.string_header_list_cities),
				TYPE_HEADER_EXP_TOWN);
		getActivity().getSupportLoaderManager().initLoader(ID_CURSOR_LOADER_PARK_TABLE_OPT, null, this);
		mExpandable = (ExpandableListView) view.findViewById(R.id.expandableListOption);
		mExpandable.setDivider(null);
		mExpandable.setOnGroupClickListener(this);
		mExpandable.setGroupIndicator(null);
		this.initializeAdapter();
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}

	private void initializeAdapter() {


		ExpandableGroupe groupePM = new ExpandableGroupe(
				getActivity().getResources().getString(R.string.string_header_list_close_user), TYPE_HEADER_SEARCH);
		ArrayList<ExpandableObject> arrayPM = new ArrayList<ExpandableObject>();
		groupePM.setmArray(arrayPM);
		
		ExpandableGroupe groupePDest = new ExpandableGroupe(
				getActivity().getResources().getString(R.string.string_header_list_close_destination),
				TYPE_HEADER_EXP_SEARCH_DEST);
		ArrayList<ExpandableObject> arrayPDest = new ArrayList<ExpandableObject>();
		groupePDest.setmArray(arrayPDest);
		
		mArrayGroupe.add(groupePM);
		mArrayGroupe.add(groupePDest);

		ExpandableGroupe categorie = new ExpandableGroupe(
				getActivity().getResources().getString(R.string.string_header_list_categories), TYPE_HEADER_EXP_CAT);
		ArrayList<ExpandableObject> arrayCate = new ArrayList<ExpandableObject>();
		arrayCate.add(new ExpandableObject(getActivity().getResources().getString(R.string.string_name_cat_car),
				categorie, TYPE_HEADER_SEARCH_CAT, R.drawable.icon_car_search));
		arrayCate.add(new ExpandableObject(getActivity().getResources().getString(R.string.string_name_cat_bus),
				categorie, TYPE_HEADER_SEARCH_CAT, R.drawable.icon_bus_search));
		arrayCate.add(new ExpandableObject(getActivity().getResources().getString(R.string.string_name_cat_campeur),
				categorie, TYPE_HEADER_SEARCH_CAT, R.drawable.icon_camp_search));
		categorie.setmArray(arrayCate);
		mArrayGroupe.add(categorie);

		mAdapter = new ExpandableListAdapter(getActivity(), mArrayGroupe, (BaseGuiActivity)getActivity());
		mExpandable.setAdapter(mAdapter);
	}

	@Override
	public boolean onGroupClick(ExpandableListView parent, View v,
			int groupPosition, long id) {

		if (groupPosition == 0) {
			((BaseGuiActivity)getActivity()).searchAroundLocation(groupPosition, null);
			return true;
		}
		else if (groupPosition == 1) {
			((BaseGuiActivity)getActivity()).handleSearchView();
			return true;
		}
		return false;
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle arg1) {

		switch (id) {
		case ID_CURSOR_LOADER_PARK_TABLE_OPT :

			return new CursorLoader(getActivity(), PARK_TABLE.CONTENT_URI, PARK_TABLE.PROJECTION, null, null, null);
		}
		return null;
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {

		switch (loader.getId()) {
		case ID_CURSOR_LOADER_PARK_TABLE_OPT:

			if (cursor.moveToFirst()) {
				do {
					if (mArrayTown.size() == 0)
						mArrayTown.add(new ExpandableObject(cursor.getString
								(cursor.getColumnIndex(PARK_TABLE.Columns.VILLE.getName())), mTown, TYPE_HEADER_SEARCH_TOWN));	
					boolean ret = false;
					for (int idx = 0; idx < mArrayTown.size(); ++idx) {
						if (mArrayTown.get(idx).getmName().
								contentEquals(cursor.getString(cursor.getColumnIndex(PARK_TABLE.Columns.VILLE.getName()))))
							ret = true;
					}
					if (!ret)
						mArrayTown.add(new ExpandableObject(cursor.getString
								(cursor.getColumnIndex(PARK_TABLE.Columns.VILLE.getName())), mTown, TYPE_HEADER_SEARCH_TOWN));
				} while (cursor.moveToNext());

				mTown.setmArray(mArrayTown);
				if (mArrayGroupe.contains(mTown))
					mArrayGroupe.remove(mTown);
				mArrayGroupe.add(mTown);
				mAdapter.setArrayGroup(mArrayGroupe);
			}

			break;
		}
	}

	@Override
	public void onLoaderReset(Loader<Cursor> arg0) {
	}

	@Override
	public boolean onChildClick(ExpandableListView parent, View v,
			int groupPosition, int childPosition, long id) {
		return false;
	}

	@Override
	public void onSearchDestination(String dest, int group) {

		LogApp.e("SlidingFragment :", "Destination : " + dest);
		((BaseGuiActivity)getActivity()).searchAroundLocation(group, dest);
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		mExpandable.setOnGroupClickListener(null);
		mTitleSearchOption = null;
		mAdapter = null;
		mExpandable = null;
		for (int idx = 0; idx < mArrayGroupe.size(); ++idx)
			mArrayGroupe.remove(idx);
		for (int idx = 0; idx < mArrayTown.size(); ++idx)
			mArrayTown.remove(idx);
		mArrayGroupe = null;
		mArrayTown = null;
		mTown = null;
	}
}
