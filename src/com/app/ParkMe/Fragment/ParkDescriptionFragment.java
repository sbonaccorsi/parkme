package com.app.ParkMe.Fragment;

import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.app.ParkMe.R;
import com.app.ParkMe.Database.data.provider.HelpMeToParkMeContent.PARK_TABLE;
import com.app.ParkMe.Utils.Constant;

public class ParkDescriptionFragment extends SherlockFragment implements Constant {

	private TextView mNamePark;
	private TextView mAddrPark;
	private TextView mCodePVille;
	private TextView mTel;
	private TextView mMail;
	private TextView mWeb;
	
	private Typeface mFont;

	private boolean mIsAlreadyStart = false;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_park, container, false);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		mFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/chunkfive_ex.ttf");
		
		String name = getActivity().getIntent().getExtras().getString("namePark");
//		String name = "Test parking Marseille";
		
		((TextView)view.findViewById(R.id.title_addr)).setTypeface(mFont);
		((TextView)view.findViewById(R.id.title_contacts)).setTypeface(mFont);
		
		mNamePark = (TextView) view.findViewById(R.id.name_park_desc);
		mNamePark.setTypeface(mFont);
		
		mAddrPark = (TextView) view.findViewById(R.id.addr_park);
		mCodePVille = (TextView) view.findViewById(R.id.codePostal_ville);
		mTel = (TextView) view.findViewById(R.id.contact_tel);
		mMail = (TextView) view.findViewById(R.id.contact_mail);
		mWeb = (TextView) view.findViewById(R.id.contact_web);
		
		mAddrPark.setTypeface(mFont);
		mCodePVille.setTypeface(mFont);
		mTel.setTypeface(mFont);
		mMail.setTypeface(mFont);
		mWeb.setTypeface(mFont);

		mNamePark.setText(name);

		Cursor cursor = getActivity().getContentResolver().query(PARK_TABLE.CONTENT_URI, PARK_TABLE.PROJECTION, 
				PARK_TABLE.Columns.RAISON_SOCIAL.getName() + "=?", new String[]{name}, null);

		if (cursor.moveToFirst()) {

			this.initializeAddr(cursor);
			this.initializeCodePostalVille(cursor);
			this.initializeContacts(cursor);
		}
		cursor.close();
		
		Linkify.addLinks(mTel, Linkify.PHONE_NUMBERS);
		Linkify.addLinks(mMail, Linkify.EMAIL_ADDRESSES);
		Linkify.addLinks(mWeb, Linkify.WEB_URLS);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		if (!mIsAlreadyStart) {
			mIsAlreadyStart = true;
		}
	}
	
	private void initializeAddr(Cursor cursor) {

		String str = null;

		if (!cursor.getString(cursor.getColumnIndex(PARK_TABLE.Columns.LIGNE_ADDR_1.getName())).contentEquals(""))
			mAddrPark.setText(cursor.getString(cursor.getColumnIndex(PARK_TABLE.Columns.LIGNE_ADDR_1.getName())));

		if (!cursor.getString(cursor.getColumnIndex(PARK_TABLE.Columns.NUMERO.getName())).contentEquals("")) {
			if (!mAddrPark.getText().toString().contentEquals(""))
				str = mAddrPark.getText().toString() + " " + cursor.getString(cursor.getColumnIndex(PARK_TABLE.Columns.NUMERO.getName()));
			else
				str = cursor.getString(cursor.getColumnIndex(PARK_TABLE.Columns.NUMERO.getName()));
			mAddrPark.setText(str);
		}

		if (!cursor.getString(cursor.getColumnIndex(PARK_TABLE.Columns.TYPE_VOIE.getName())).contentEquals("")) {
			if (!mAddrPark.getText().toString().contentEquals(""))
				str = mAddrPark.getText().toString() + " " + cursor.getString(cursor.getColumnIndex(PARK_TABLE.Columns.TYPE_VOIE.getName()));
			else
				str = cursor.getString(cursor.getColumnIndex(PARK_TABLE.Columns.TYPE_VOIE.getName()));
			mAddrPark.setText(str);
		}

		if (!cursor.getString(cursor.getColumnIndex(PARK_TABLE.Columns.VOIE.getName())).contentEquals("")) {
			if (!mAddrPark.getText().toString().contentEquals(""))
				str = mAddrPark.getText().toString() + " " + cursor.getString(cursor.getColumnIndex(PARK_TABLE.Columns.VOIE.getName()));
			else
				str = cursor.getString(cursor.getColumnIndex(PARK_TABLE.Columns.VOIE.getName()));
			mAddrPark.setText(str);
		}

		if (!cursor.getString(cursor.getColumnIndex(PARK_TABLE.Columns.LIGNE_ADDR_3.getName())).contentEquals("")) {
			if (!mAddrPark.getText().toString().contentEquals(""))
				str = mAddrPark.getText().toString() + " " + cursor.getString(cursor.getColumnIndex(PARK_TABLE.Columns.LIGNE_ADDR_3.getName()));
			else
				str = cursor.getString(cursor.getColumnIndex(PARK_TABLE.Columns.LIGNE_ADDR_3.getName()));
			mAddrPark.setText(str);
		}

		if (mAddrPark.getText().toString().length() == 0)
			mAddrPark.setVisibility(View.GONE);
	}

	private void initializeCodePostalVille(Cursor cursor) {

		if (!cursor.getString(cursor.getColumnIndex(PARK_TABLE.Columns.CODE_POSTAL.getName())).contentEquals(""))
			mCodePVille.setText(cursor.getString(cursor.getColumnIndex(PARK_TABLE.Columns.CODE_POSTAL.getName())));

		if (!cursor.getString(cursor.getColumnIndex(PARK_TABLE.Columns.VILLE.getName())).contentEquals(""))
			mCodePVille.setText(mCodePVille.getText().toString() + " " +
					cursor.getString(cursor.getColumnIndex(PARK_TABLE.Columns.VILLE.getName())));
	}

	private void initializeContacts(Cursor cursor) {

		if (!cursor.getString(cursor.getColumnIndex(PARK_TABLE.Columns.TELEPHONE.getName())).contentEquals(""))
			mTel.setText(cursor.getString(cursor.getColumnIndex(PARK_TABLE.Columns.TELEPHONE.getName())));
		else
			mTel.setVisibility(View.GONE);

		if (!cursor.getString(cursor.getColumnIndex(PARK_TABLE.Columns.MAIL.getName())).contentEquals(""))
			mMail.setText(cursor.getString(cursor.getColumnIndex(PARK_TABLE.Columns.MAIL.getName())));
		else
			mMail.setVisibility(View.GONE);

		if (!cursor.getString(cursor.getColumnIndex(PARK_TABLE.Columns.ADDR_WEB.getName())).contentEquals(""))
			mWeb.setText(cursor.getString(cursor.getColumnIndex(PARK_TABLE.Columns.ADDR_WEB.getName())));
		else
			mWeb.setVisibility(View.GONE);
	}
}
