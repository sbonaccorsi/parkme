package com.app.ParkMe.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.app.ParkMe.R;
import com.app.ParkMe.Activity.SelectVehicleActivity;

public class BusFragment extends Fragment implements OnClickListener {

	private ImageView mImg;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragement_bus, container, false);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		mImg = (ImageView) view.findViewById(R.id.image_bus_fragment);
		mImg.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {

		if (mImg == v) {
			((SelectVehicleActivity)getActivity()).startIntentMap();
		}
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		mImg = null;
	}
}
