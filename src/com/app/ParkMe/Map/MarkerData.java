package com.app.ParkMe.Map;

import com.google.android.gms.maps.model.Marker;

public class MarkerData {

	private double mDist;
	private Marker mMarker;
	private Marker mMarkerDestination;
	private String mEntityId;
	private String mTown;

	public MarkerData() {

	}

	public String getmEntityId() {
		return mEntityId;
	}

	public void setmEntityId(String mEntityId) {
		this.mEntityId = mEntityId;
	}
	
	public double getmDist() {
		return mDist;
	}

	public void setmDist(double mDist) {
		this.mDist = mDist;
	}

	public Marker getmMarker() {
		return mMarker;
	}

	public void setmMarker(Marker mMarker) {
		this.mMarker = mMarker;
	}
	
	public String getmTown() {
		return mTown;
	}

	public void setmTown(String mTown) {
		this.mTown = mTown;
	}
	
	public void setMarkerDestination(Marker dest) {
		mMarkerDestination = dest;
	}
	
	public Marker getMarkerDestination() {
		return mMarkerDestination;
	}
}
