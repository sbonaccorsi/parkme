package com.app.ParkMe.Map;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.app.ParkMe.R;
import com.app.ParkMe.Fragment.MapGoogleFragment;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.model.Marker;

public class CustomMarker implements InfoWindowAdapter {

	private Context mContext;
	private MapGoogleFragment mMapFragment;
	
	public CustomMarker(Context context, MapGoogleFragment map) {
	
		mContext = context;
		mMapFragment = map;
	}
	
	@Override
	public View getInfoContents(Marker marker) {
		
		LayoutInflater inflate = LayoutInflater.from(mContext);
		View v = inflate.inflate(R.layout.view_custom_marker, null);
		
		ArrayList<MarkerData> array = mMapFragment.getArrayDestination();
		MarkerData data = null;
		for (int idx = 0; idx < array.size(); ++idx) {
			if (marker.getPosition().latitude == array.get(idx).getmMarker().getPosition().latitude &&
					marker.getPosition().longitude == array.get(idx).getmMarker().getPosition().longitude) {
				data = array.get(idx);
			}
		}
		if (data != null) {
			((TextView)v.findViewById(R.id.textView_title_marker)).setText(data.getmMarker().getTitle());
			((TextView)v.findViewById(R.id.textView_by_dest)).setVisibility(View.VISIBLE);
		}
		else {
			((TextView)v.findViewById(R.id.textView_title_marker)).setText(marker.getTitle());
			((TextView)v.findViewById(R.id.textView_by_dest)).setVisibility(View.GONE);
		}
		return v;
	}

	@Override
	public View getInfoWindow(Marker marker) {

		return null;
	}

}
