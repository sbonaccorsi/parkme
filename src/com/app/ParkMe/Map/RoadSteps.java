package com.app.ParkMe.Map;

public class RoadSteps {

	private double mLatStart;
	private double mLonStart;
	private double mLatEnd;
	private double mLonEnd;
	private String mIntruction;
	private String mDist;
	private String mTime;
	
	public RoadSteps() {
		
	}
	
	public double getmLatStart() {
		return mLatStart;
	}

	public void setmLatStart(double mLatStart) {
		this.mLatStart = mLatStart;
	}

	public double getmLonStart() {
		return mLonStart;
	}

	public void setmLonStart(double mLonStart) {
		this.mLonStart = mLonStart;
	}

	public double getmLatEnd() {
		return mLatEnd;
	}

	public void setmLatEnd(double mLatEnd) {
		this.mLatEnd = mLatEnd;
	}

	public double getmLonEnd() {
		return mLonEnd;
	}

	public void setmLonEnd(double mLonEnd) {
		this.mLonEnd = mLonEnd;
	}

	public String getmIntruction() {
		return mIntruction;
	}

	public void setmIntruction(String mIntruction) {
		this.mIntruction = mIntruction;
	}

	public String getmDist() {
		return mDist;
	}

	public void setmDist(String mDist) {
		this.mDist = mDist;
	}

	public String getmTime() {
		return mTime;
	}

	public void setmTime(String mTime) {
		this.mTime = mTime;
	}
}
