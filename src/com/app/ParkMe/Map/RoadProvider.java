package com.app.ParkMe.Map;

import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;

import com.app.ParkMe.Database.data.provider.HelpMeToParkMeContent.ROAD_TABLE;
import com.app.ParkMe.Utils.Constant;
import com.app.ParkMe.Utils.LogApp;

public class RoadProvider implements Constant {

	// TODO Add ans commit file

	public static String getUrlRoadMap(double fromLat, double fromLon, double toLat, double toLon) {

		StringBuffer urlString = new StringBuffer();
		urlString.append(URL_BASE_ROAD);
		urlString.append("origin=");// from
		urlString.append(Double.toString(fromLat));
		urlString.append(",");
		urlString.append(Double.toString(fromLon));
		urlString.append("&destination=");// to
		urlString.append(Double.toString(toLat));
		urlString.append(",");
		urlString.append(Double.toString(toLon));
		urlString.append("&sensor=false");
		urlString.append("&language=" + Locale.getDefault().getLanguage());
		
		LogApp.e("RoadProvider :", "Current langage : " + Locale.getDefault().getLanguage());
		
		return urlString.toString();
	}

	public static Road parseRoad(String rep, Context context) {

		Road road = new Road();
		String distT = null;
		String timeT = null;
		try {
			JSONObject json = new JSONObject(rep);

			if (json.getString("status").contentEquals("OK")) {

				JSONArray jsonArrayRoute = json.getJSONArray("routes");
				for (int idxRoute = 0; idxRoute < jsonArrayRoute.length(); ++idxRoute) {

					JSONObject jsonObj = jsonArrayRoute.getJSONObject(idxRoute);
					JSONArray jsonArrayLegs = jsonObj.getJSONArray("legs");
					for (int idxLegs = 0; idxLegs < jsonArrayLegs.length(); ++idxLegs) {

						JSONObject jsonObjLegs = jsonArrayLegs.getJSONObject(idxLegs);
						if (jsonObjLegs.has("distance")) {
							distT = jsonObjLegs.getJSONObject("distance").getString("text");
							road.setmDistTotal(distT);
						}
						if (jsonObjLegs.has("duration")) {
							timeT = jsonObjLegs.getJSONObject("duration").getString("text");
							road.setmTimeTotal(timeT);
						}
						if (jsonObjLegs.has("start_address"))
							road.setmStartAddr(jsonObjLegs.getString("start_address"));
						if (jsonObjLegs.has("end_address"))
							road.setmEndAddr(jsonObjLegs.getString("end_address"));
						if (jsonObjLegs.has("start_location")) {
							road.setmStartLat(jsonObjLegs.getJSONObject("start_location").getDouble("lat"));
							road.setmStartLon(jsonObjLegs.getJSONObject("start_location").getDouble("lng"));
						}
						if (jsonObjLegs.has("end_location")) {
							road.setmEndLat(jsonObjLegs.getJSONObject("end_location").getDouble("lat"));
							road.setmEndLon(jsonObjLegs.getJSONObject("end_location").getDouble("lng"));
						}

						if (jsonObjLegs.has("steps")) {
							JSONArray jsonArraySteps = jsonObjLegs.getJSONArray("steps");
							for (int idxSteps = 0; idxSteps < jsonArraySteps.length(); ++idxSteps) {
								
								ContentValues values = new ContentValues();
								values.put(ROAD_TABLE.Columns.DISTANCE_TOTAL.getName(), distT);
								values.put(ROAD_TABLE.Columns.DURATION_TOTAL.getName(), timeT);
								
								JSONObject jsonObjSteps = jsonArraySteps.getJSONObject(idxSteps);
								RoadSteps steps = new RoadSteps();
								if (jsonObjSteps.has("start_location")) {
									double lat = jsonObjSteps.getJSONObject("start_location").getDouble("lat");
									double lon = jsonObjSteps.getJSONObject("start_location").getDouble("lng");
									values.put(ROAD_TABLE.Columns.LATITUDE_START.getName(), ""+lat);
									values.put(ROAD_TABLE.Columns.LONGITUDE_START.getName(), ""+lon);
									steps.setmLatStart(lat);
									steps.setmLonStart(lon);
								}
								if (jsonObjSteps.has("end_location")) {
									double lat = jsonObjSteps.getJSONObject("end_location").getDouble("lat");
									double lon = jsonObjSteps.getJSONObject("end_location").getDouble("lng");
									values.put(ROAD_TABLE.Columns.LATITUDE_END.getName(), ""+lat);
									values.put(ROAD_TABLE.Columns.LONGITUDE_END.getName(), ""+lon);
									steps.setmLatEnd(lat);
									steps.setmLonEnd(lon);
								}
								if (jsonObjSteps.has("distance")) {
									String dist = jsonObjSteps.getJSONObject("distance").getString("text");
									values.put(ROAD_TABLE.Columns.DISTANCE.getName(), dist);
									steps.setmDist(dist);
								}
								if (jsonObjSteps.has("duration")) {
									String dur = jsonObjSteps.getJSONObject("duration").getString("text");
									values.put(ROAD_TABLE.Columns.DURATION.getName(), dur);
									steps.setmTime(dur);
								}
								if (jsonObjSteps.has("html_instructions")) {
									String inst = jsonObjSteps.getString("html_instructions");
									values.put(ROAD_TABLE.Columns.INSTRUCTION.getName(), inst);
									steps.setmIntruction(inst);
								}
								context.getContentResolver().insert(ROAD_TABLE.CONTENT_URI, values);
								road.addSteps(steps);
							}
						}
					}
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return road;
	}
}