package com.app.ParkMe.Map;

import java.util.ArrayList;

public class Road extends Object {

	private ArrayList<RoadSteps> mArraySteps;
	
	// Data legs
	private String mStartAddr;
	private String mEndAddr;
	private String mDistTotal;
	private String mTimeTotal;
	private double mStartLat;
	private double mStartLon;
	private double mEndLat;
	private double mEndLon;
	
	public Road() {
		
		mArraySteps = new ArrayList<RoadSteps>();
	}
	
	public ArrayList<RoadSteps> getArrayCoord() {
		return mArraySteps;
	}
	
	public void setArrayCoord(ArrayList<RoadSteps> arrayCoord) {
		this.mArraySteps = arrayCoord;
	}
	
	public void addSteps(RoadSteps coord) {
		mArraySteps.add(coord);
	}
	
	public String getmStartAddr() {
		return mStartAddr;
	}
	
	public void setmStartAddr(String mStartAddr) {
		this.mStartAddr = mStartAddr;
	}
	
	public String getmEndAddr() {
		return mEndAddr;
	}
	
	public void setmEndAddr(String mEndAddr) {
		this.mEndAddr = mEndAddr;
	}
	
	public String getmDistTotal() {
		return mDistTotal;
	}
	
	public void setmDistTotal(String mDistTotal) {
		this.mDistTotal = mDistTotal;
	}
	
	public String getmTimeTotal() {
		return mTimeTotal;
	}
	
	public void setmTimeTotal(String mTimeTotal) {
		this.mTimeTotal = mTimeTotal;
	}
	
	public double getmStartLat() {
		return mStartLat;
	}
	
	public void setmStartLat(double mStartLat) {
		this.mStartLat = mStartLat;
	}
	
	public double getmStartLon() {
		return mStartLon;
	}
	
	public void setmStartLon(double mStartLon) {
		this.mStartLon = mStartLon;
	}
	
	public double getmEndLat() {
		return mEndLat;
	}
	
	public void setmEndLat(double mEndLat) {
		this.mEndLat = mEndLat;
	}
	
	public double getmEndLon() {
		return mEndLon;
	}
	
	public void setmEndLon(double mEndLon) {
		this.mEndLon = mEndLon;
	}
	
}
