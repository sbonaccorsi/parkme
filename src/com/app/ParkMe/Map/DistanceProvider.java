package com.app.ParkMe.Map;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.app.ParkMe.Utils.Constant;

public class DistanceProvider implements Constant {
	
	public static String getBaseUrl_StartPosition(double latS, double lonS) {
		
		StringBuffer urlString = new StringBuffer();
		urlString.append(URL_BASE_DISTANCE);
		urlString.append("origins=");// from
		urlString.append(Double.toString(latS));
		urlString.append(",");
		urlString.append(Double.toString(lonS));
		return urlString.toString();
	}
	
	public static String putDestinationParam(String url) {
		
		StringBuffer urlString = new StringBuffer(url);
		urlString.append("&destinations=");
		return urlString.toString();
	}
	
	public static String putSeparator(String url, String separator) {
		
		StringBuffer urlString = new StringBuffer(url);
		urlString.append(separator);
		return urlString.toString();
	}
	
	public static String makeUrlWithDestination(String url, double latD, double lonD) {
		
		StringBuffer urlString = new StringBuffer(url);
		urlString.append(Double.toString(latD));
		urlString.append(",");
		urlString.append(Double.toString(lonD));
		return urlString.toString();
	}
	
	public static String putParameterSensor(String url, String value) {
		
		StringBuffer urlString = new StringBuffer(url);
		urlString.append("&sensor=" + value);
		return urlString.toString();
	}
	
	public static ArrayList<MarkerData> parseResponce(String rep, ArrayList<MarkerData> array) {
		
		try {
			JSONObject json = new JSONObject(rep);
			if (json.has("rows")) {
				JSONArray arrayRows = json.getJSONArray("rows");
				
				for (int idxRows = 0; idxRows < arrayRows.length(); ++idxRows) {
					
					JSONObject jsonRows = arrayRows.getJSONObject(idxRows);
					if (jsonRows.has("elements")) {
						
						JSONArray arrayElements = jsonRows.getJSONArray("elements");
						for (int idxElem = 0; idxElem < arrayElements.length(); ++idxElem) {
							
							JSONObject jsonElem = arrayElements.getJSONObject(idxElem);
							if (json.getString("status").contentEquals("OK")) {
								JSONObject jsonDist = jsonElem.getJSONObject("distance");
								double dist = jsonDist.getInt("value");
								array.get(idxElem).setmDist(dist);
							}
						}
					}
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return array;
	}
	
}
