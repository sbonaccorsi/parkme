package com.app.ParkMe.Map;

import android.location.Address;

public class AddressData {

	private Address mAddr;
	private boolean mSelected;
	
	public AddressData(Address addr) {
		mAddr = addr;
		mSelected = false;
	}

	public Address getmAddr() {
		return mAddr;
	}

	public void setmAddr(Address mAddr) {
		this.mAddr = mAddr;
	}

	public boolean ismSelected() {
		return mSelected;
	}

	public void setmSelected(boolean mSelected) {
		this.mSelected = mSelected;
	}
}
