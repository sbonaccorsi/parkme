package com.app.ParkMe.Application;

import org.acra.ACRA;
import org.acra.annotation.ReportsCrashes;

import android.app.Application;

@ReportsCrashes(formKey = "dGdJNGVoS0ZmLW16YklLaHpGNG9yclE6MQ") 
public class ParkMeApplication extends Application {

	@Override
	public void onCreate() {
		super.onCreate();
		
		ACRA.init(this);
	}
}
