package com.app.ParkMe.Algorithm;

import java.util.Comparator;

import com.app.ParkMe.Map.MarkerData;

public class ComparatorDist implements Comparator<MarkerData> {

	@Override
	public int compare(MarkerData lhs, MarkerData rhs) {

		if (lhs.getmDist() < rhs.getmDist())
			return -1;
		else if (lhs.getmDist() > rhs.getmDist())
			return 1;
		else
			return 0;
	}

}
