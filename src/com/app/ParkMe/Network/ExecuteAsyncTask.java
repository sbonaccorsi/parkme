package com.app.ParkMe.Network;

import java.util.ArrayList;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.app.ParkMe.DBManagment.DBManagment;
import com.app.ParkMe.Map.DistanceProvider;
import com.app.ParkMe.Map.MarkerData;
import com.app.ParkMe.Map.Road;
import com.app.ParkMe.Map.RoadProvider;
import com.app.ParkMe.Parser.ParserXML;
import com.app.ParkMe.Utils.Constant;
import com.app.ParkMe.Utils.LogApp;

public class ExecuteAsyncTask extends AsyncTask<TaskManager, Void, AnswerData> implements Constant {

	private Context mContext;
	private TaskManager mTask;

	@Override
	protected AnswerData doInBackground(TaskManager... params) {

		ResponseHttp rep = null;
		AnswerData answer = null;
		mTask = params[0];
		mContext = mTask.getContext();
		switch (mTask.getTypeRequest()) {
		case REQUEST_TYPE_BASE:
			rep = HttpRequest.getInstance(mContext).httpRequest_GET(mTask.getUrl());
			LogApp.e("ExecuteAsyncTask :", mTask.getUrl());
			LogApp.e("ExecuteAsyncTask :", "REQUEST_TYPE_BASE Status Code " + rep.getStatus());
			if (rep.getStatus() == 200) {
				Document doc = ParserXML.getInstance().getDomElement(rep.getResponse());
				NodeList listNode = doc.getElementsByTagName("content");
				if (isCancelled() == false)
					DBManagment.getInstance(mContext).parseListNode(listNode);
				answer = new AnswerData(REQUEST_TYPE_BASE, GOOD_ANSWER, rep.getStatus(), null, mTask);
			}
			else
				answer = new AnswerData(REQUEST_TYPE_BASE, BAD_ANSWER, rep.getStatus(), null, mTask);
			break;

		case REQUEST_TYPE_ROAD :
			rep = HttpRequest.getInstance(mContext).httpRequest_GET(mTask.getUrl());
			LogApp.e("ExecuteAsyncTask :", mTask.getUrl());
			LogApp.e("ExecuteAsyncTask :", "REQUEST_TYPE_ROAD Status Code " + rep.getStatus());
			if (rep.getStatus() == 200) {
				Road road = null;
				if (isCancelled() == false)
					road = RoadProvider.parseRoad(rep.getResponse(), mTask.getContext());
				answer = new AnswerData(REQUEST_TYPE_ROAD, GOOD_ANSWER, rep.getStatus(), road, mTask);
			} 
			else
				answer = new AnswerData(REQUEST_TYPE_ROAD, BAD_ANSWER, rep.getStatus(), null, mTask);
			break;

		case REQUEST_TYPE_DISTANCE :
			rep = HttpRequest.getInstance(mContext).httpRequest_GET(mTask.getUrl());
			LogApp.e("ExecuteAsyncTask :", mTask.getUrl());
			LogApp.e("ExecuteAsyncTask :", "REQUEST_TYPE_DISTANCE Status Code " + rep.getStatus());
			if (rep.getStatus() == 200) {
				ArrayList<MarkerData> array = null;
				if (isCancelled() == false)
					array = DistanceProvider.parseResponce(rep.getResponse(), 
							mTask.getParam());
				answer = new AnswerData(REQUEST_TYPE_DISTANCE, GOOD_ANSWER, rep.getStatus(), array, mTask);
			}
			else
				answer = new AnswerData(REQUEST_TYPE_DISTANCE, BAD_ANSWER, rep.getStatus(), null, mTask);

			break;
		}
		if (isCancelled() == false)
			return answer;
		else
			return null;
	}

	@Override
	protected void onPostExecute(AnswerData result) {
		super.onPostExecute(result);

		if (result != null) {
			mTask.setStateTask(TaskQueue.TASK_FINISH);
			TaskQueue.getInstance().removeTask(mTask);
			if (isCancelled() == false) {
				if (mTask.getListener() != null)
					mTask.getListener().onRequestIsComplete(result);
			}
			else {
				LogApp.e("ExecuteAsyncTask :", mTask.getUrl() + " [Task is canceled]");
				TaskQueue.getInstance().clearQueue();
			}
		}
		else {
			LogApp.e("ExecuteAsyncTask :", mTask.getUrl() + " [Task is canceled]");
			TaskQueue.getInstance().clearQueue();
		}
	}
}
