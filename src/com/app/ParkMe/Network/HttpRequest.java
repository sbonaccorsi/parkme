package com.app.ParkMe.Network;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

import com.app.ParkMe.Utils.Constant;

import android.content.Context;
import android.util.Log;

public class HttpRequest implements Constant {

	private static HttpRequest mInstance;

	public static HttpRequest getInstance(Context ctx) {

		if (mInstance == null) {
			mInstance = new HttpRequest(ctx);
		}
		return mInstance;
	}

	private HttpRequest(Context ctx) {

	}

	public ResponseHttp httpRequest_POST(String url, JSONObject json) {

		String response = "";
		ResponseHttp responseObject = null;
		try {

			HttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(url);

			if (json != null) {
				StringEntity se = new StringEntity(json.toString());
				httpPost.setHeader(HTTP.CONTENT_TYPE, "application/json");
				httpPost.setHeader("Accept", "application/json");
				httpPost.setEntity(se);
			}

			HttpResponse rep = httpClient.execute(httpPost);
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			rep.getEntity().writeTo(out);
			response = out.toString();
			responseObject = new ResponseHttp(response, rep.getStatusLine().getStatusCode());

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			responseObject = new ResponseHttp(e.getMessage(), REQUEST_RESPONSE_NO_NETWORK);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			responseObject = new ResponseHttp(e.getMessage(), REQUEST_RESPONSE_NO_NETWORK);
		} catch (IOException e) {
			e.printStackTrace();
			responseObject = new ResponseHttp(e.getMessage(), REQUEST_RESPONSE_NO_NETWORK);
		} 
		return responseObject;
	}

	public ResponseHttp httpRequest_GET(String url) {

		ResponseHttp responseHttp = null;

		try {

			HttpClient httpClient = new DefaultHttpClient();
			HttpResponse httpResponse = httpClient.execute(new HttpGet(url));

			ByteArrayOutputStream out = new ByteArrayOutputStream();
			httpResponse.getEntity().writeTo(out);
			out.close();
			String rep = out.toString();
			responseHttp = new ResponseHttp(rep, httpResponse.getStatusLine().getStatusCode());
			return responseHttp;

		} catch (ClientProtocolException e) {
			e.printStackTrace();
			responseHttp = new ResponseHttp(e.getMessage(), Constant.REQUEST_RESPONSE_NO_NETWORK);
		} catch (IOException e) {
			e.printStackTrace();
			responseHttp = new ResponseHttp(e.getMessage(), Constant.REQUEST_RESPONSE_NO_NETWORK);
		}
		return responseHttp;
	}
	
	public ResponseHttp httpRequest_GET_inputStream(String url) {

		ResponseHttp responseHttp = null;

		try {

			HttpClient httpClient = new DefaultHttpClient();
			HttpResponse httpResponse = httpClient.execute(new HttpGet(url));

			InputStream in = httpResponse.getEntity().getContent();
			responseHttp = new ResponseHttp(in, httpResponse.getStatusLine().getStatusCode());
			return responseHttp;

		} catch (ClientProtocolException e) {
			e.printStackTrace();
			responseHttp = new ResponseHttp(e.getMessage(), Constant.REQUEST_RESPONSE_NO_NETWORK);
		} catch (IOException e) {
			e.printStackTrace();
			responseHttp = new ResponseHttp(e.getMessage(), Constant.REQUEST_RESPONSE_NO_NETWORK);
		}
		return responseHttp;
	}
	
}
