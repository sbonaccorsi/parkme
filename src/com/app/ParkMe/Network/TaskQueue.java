package com.app.ParkMe.Network;

import java.util.ArrayList;

public class TaskQueue {

	public static final int TASK_NOT_SEND = 1;
	public static final int TASK_SEND = 2;
	public static final int TASK_FINISH = 3;
	
	private ArrayList<TaskManager> mQueueTask;
	private ArrayList<ExecuteAsyncTask> mQueueAsync;
	private ExecuteAsyncTask mCurrentAsync = null;
	
	static private TaskQueue mInstance;
	
	public static TaskQueue getInstance() {
		
		if (mInstance == null) {
			mInstance = new TaskQueue();
		}
		return mInstance;
	}
	
	private TaskQueue() {
		 
		mQueueTask = new ArrayList<TaskManager>();
		mQueueAsync = new ArrayList<ExecuteAsyncTask>();
	}
	
	public void clearQueue() {
		mQueueTask.clear();
		mQueueAsync.clear();
	}
	
	public void addTaskInQueue(TaskManager task) {
		
		task.setStateTask(TASK_NOT_SEND);
		mQueueTask.add(task);
		this.performTask();
	}
	
	public void removeTask(TaskManager task) {
		
		if (task.getStateTask() == TASK_FINISH) {
			
			if (mQueueTask.size() > 0 && mQueueAsync.size() > 0) {
				mQueueTask.remove(task);
				mQueueAsync.remove(mCurrentAsync);
				mCurrentAsync = null;
			}
		}
		this.performTask();
	}
	
	public void cancelTask(boolean mayInterupt) {
		mCurrentAsync.cancel(mayInterupt);
	}
	
	public void cancelAllTask(boolean mayInterupt) {
		
		for (int idx = 0; idx < mQueueAsync.size(); idx++) {
			mQueueAsync.get(idx).cancel(mayInterupt);
		}
	}
	
	public ExecuteAsyncTask getCurrentTask() {
		return mCurrentAsync;
	}
	
	private void performTask() {
		
		if (mQueueTask.size() != 0) {
			
			if (mQueueTask.get(0).getStateTask() == TASK_NOT_SEND) {

				mQueueTask.get(0).setStateTask(TASK_SEND);
				ExecuteAsyncTask async = new ExecuteAsyncTask();
				mQueueAsync.add(async);
				mCurrentAsync = async;
				async.execute(mQueueTask.get(0));
			}
		}
	}
}
