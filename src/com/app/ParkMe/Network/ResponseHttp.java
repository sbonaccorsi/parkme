package com.app.ParkMe.Network;

import java.io.InputStream;

public class ResponseHttp {

	private int mStatus;
	private String mResponse;
	private InputStream mIn;

	public ResponseHttp(String response, int statusCode) {

		mStatus = statusCode;
		mResponse = response;
	}

	public ResponseHttp(InputStream response, int statusCode) {

		mStatus = statusCode;
		mIn = response;
	}

	public InputStream getInputStream() {
		
		return mIn;
	}
	
	public int getStatus() {

		return mStatus;
	}

	public String getResponse() {

		return mResponse;
	}

}
