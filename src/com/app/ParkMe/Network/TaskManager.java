package com.app.ParkMe.Network;

import java.util.ArrayList;

import org.json.JSONObject;

import com.app.ParkMe.Listener.HttpRequestListener;
import com.app.ParkMe.Map.MarkerData;

import android.content.Context;

public class TaskManager {

	private String mUrl;
	private int mRequestType;
	private HttpRequestListener mListener;
	private Context mContext;
	private JSONObject mJson;
	
	private ArrayList<MarkerData> mParam;
	
	private Object mData1;
	private Object mData2;
	private Object mData3;
	private Object mData4;
	
	private int mStateTask;
	
	public TaskManager(HttpRequestListener listener, Context ctx, String url, int type, JSONObject param) {
		
		mListener = listener;
		mContext = ctx;
		mUrl = url;
		mRequestType = type;
		mJson = param;
	}
	
	public HttpRequestListener getListener() {
		
		return mListener;
	}
	
	public void setParam(ArrayList<MarkerData> obj) {
		
		mParam = obj;
	}
	
	public ArrayList<MarkerData> getParam() {
		
		return mParam;
	}
	
	public Context getContext() {
		
		return mContext;
	}
	
	public String getUrl() {
		
		return mUrl;
	}
	
	public int getTypeRequest() {
		
		return mRequestType;
	}
	
	public JSONObject getJsonParam() {
		
		return mJson;
	}
	
	public void setData1(Object data) {
		mData1 = data;
	}
	
	public void setData2(Object data) {
		mData2 = data;
	}
	
	public Object getData1() {
		return mData1;
	}
	
	public Object getData2() {
		return mData2;
	}
	
	public void setData3(Object data) {
		mData3 = data;
	}
	
	public Object getData3() {
		return mData3;
	}
	
	public void setData4(Object data) {
		mData4 = data;
	}
	
	public Object getData4() {
		return mData4;
	}

	public int getStateTask() {
		return mStateTask;
	}

	public void setStateTask(int mStateTask) {
		this.mStateTask = mStateTask;
	}
}
