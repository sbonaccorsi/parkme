package com.app.ParkMe.Network;

public class AnswerData {

	private int mTypeRequest;
	private int mStatusRequest;
	private Object mData;
	private TaskManager mTask;
	private int mStatusCode;
	
	public AnswerData(int typeRequest, int statusRequest, int statusCode, Object data, TaskManager task) {
		
		mTypeRequest = typeRequest;
		mStatusRequest = statusRequest;
		mData = data;
		mTask = task;
		mStatusCode = statusCode;
	}

	public int getStatusCode() {
		return mStatusCode;
	}
	
	public int getTypeRequest() {
		return mTypeRequest;
	}

	public void setTypeRequest(int mTypeRequest) {
		this.mTypeRequest = mTypeRequest;
	}

	public int getStatusRequest() {
		return mStatusRequest;
	}

	public void setStatusRequest(int mStatusRequest) {
		this.mStatusRequest = mStatusRequest;
	}

	public Object getData() {
		return mData;
	}

	public void setData(Object mData) {
		this.mData = mData;
	}
	
	public TaskManager getTask() {
		return mTask;
	}
}
