package com.app.ParkMe.DBManagment;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.app.ParkMe.Database.data.provider.HelpMeToParkMeContent.PARK_TABLE;
import com.app.ParkMe.Parser.ParserXML;
import com.app.ParkMe.Utils.Constant;

import android.content.ContentValues;
import android.content.Context;

public class DBManagment implements Constant {

	private static DBManagment mInstance;
	
	private Context mContext;
	
	public static DBManagment getInstance(Context ctx) {
		
		if (mInstance == null)
			mInstance = new DBManagment(ctx);
		return mInstance;
	}
	
	private DBManagment(Context ctx) {
		
		mContext = ctx;
	}
	
	public void parseListNode(NodeList listNode) {
		
		ContentValues[] values = new ContentValues[listNode.getLength()];
		for (int idx = 0; idx < listNode.getLength(); ++idx) {
			
			Element elem = (Element) listNode.item(idx);
			values[idx] = DBManagment.getInstance(mContext).insertParkInBase(ParserXML.getInstance().getValue(elem, PARTITION_KEY), 
					ParserXML.getInstance().getValue(elem, ROW_KEY), ParserXML.getInstance().getValue(elem, TIMESTAMP), 
					ParserXML.getInstance().getValue(elem, ENTITY_ID), ParserXML.getInstance().getValue(elem, RAISON_SOCIAL), 
					ParserXML.getInstance().getValue(elem, TYPE), ParserXML.getInstance().getValue(elem, SOUS_TYPE),
					ParserXML.getInstance().getValue(elem, LIGNE_ADDR_1), ParserXML.getInstance().getValue(elem, NUMERO),
					ParserXML.getInstance().getValue(elem, COMPL_NUMERO), ParserXML.getInstance().getValue(elem, TYPE_DE_VOIE),
					ParserXML.getInstance().getValue(elem, VOIE), ParserXML.getInstance().getValue(elem, LIGNE_ADDR_3),
					ParserXML.getInstance().getValue(elem, BOITE_POSTALE), ParserXML.getInstance().getValue(elem, CODE_POSTAL), 
					ParserXML.getInstance().getValue(elem, VILLE), ParserXML.getInstance().getValue(elem, BUREAU_DISTRIB), 
					ParserXML.getInstance().getValue(elem, INDIC_CEDEX), ParserXML.getInstance().getValue(elem, CDEX),
					ParserXML.getInstance().getValue(elem, TELEPHONE), ParserXML.getInstance().getValue(elem, MAIL),
					ParserXML.getInstance().getValue(elem, WEB), ParserXML.getInstance().getValue(elem, LONGITUDE), 
					ParserXML.getInstance().getValue(elem, LATITUDE), idx);
		}
		mContext.getContentResolver().bulkInsert(PARK_TABLE.CONTENT_URI, values);
	}
	
	private ContentValues insertParkInBase(String PartitionKey, String RowKey, String Timestamp, 
			String entityid, String raisonsociale, String type, String sous_type, String ligneadresse1,
			String numero, String compl_num, String type_voie, String voie, String lign_addr_3, String boite_postale,
			String codepostal, String ville, String bureaudistributeur, String indicateurcdex, String cdex,
			String telephone, String mail, String web, String longitude, String latitude, int idx) {
		
		ContentValues values = new ContentValues();
		values.put(PARK_TABLE.Columns.ID.getName(), idx);
		values.put(PARK_TABLE.Columns.PARTITION_KEY.getName(), PartitionKey);
		values.put(PARK_TABLE.Columns.ROW_KEY.getName(), RowKey);
		values.put(PARK_TABLE.Columns.TIMESTAMP.getName(), Timestamp);
		values.put(PARK_TABLE.Columns.ENTITY_ID.getName(), entityid);
		values.put(PARK_TABLE.Columns.RAISON_SOCIAL.getName(), raisonsociale);
		values.put(PARK_TABLE.Columns.TYPE.getName(), type);
		values.put(PARK_TABLE.Columns.SOUS_TYPE.getName(), sous_type);
		values.put(PARK_TABLE.Columns.LIGNE_ADDR_1.getName(), ligneadresse1);
		values.put(PARK_TABLE.Columns.NUMERO.getName(), numero);
		values.put(PARK_TABLE.Columns.COMPL_NUMERO.getName(), compl_num);
		values.put(PARK_TABLE.Columns.TYPE_VOIE.getName(), type_voie);
		values.put(PARK_TABLE.Columns.VOIE.getName(), voie);
		values.put(PARK_TABLE.Columns.LIGNE_ADDR_3.getName(), lign_addr_3);
		values.put(PARK_TABLE.Columns.BOITE_POSTAL.getName(), boite_postale);
		values.put(PARK_TABLE.Columns.CODE_POSTAL.getName(), codepostal);
		values.put(PARK_TABLE.Columns.VILLE.getName(), ville);
		values.put(PARK_TABLE.Columns.BUREAU_DISTRIB.getName(), bureaudistributeur);
		values.put(PARK_TABLE.Columns.INDIC_CEDEX.getName(), indicateurcdex);
		values.put(PARK_TABLE.Columns.CEDEX.getName(), cdex);
		values.put(PARK_TABLE.Columns.TELEPHONE.getName(), telephone);
		values.put(PARK_TABLE.Columns.MAIL.getName(), mail);
		values.put(PARK_TABLE.Columns.ADDR_WEB.getName(), web);
		values.put(PARK_TABLE.Columns.LONGITUDE.getName(), longitude);
		values.put(PARK_TABLE.Columns.LATITUDE.getName(), latitude);
		
		//mContext.getContentResolver().insert(PARK_TABLE.CONTENT_URI, values);
		return values;
	}
}
