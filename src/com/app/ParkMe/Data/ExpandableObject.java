package com.app.ParkMe.Data;

import com.app.ParkMe.Listener.SearchDestinationListener;

public class ExpandableObject {

	private ExpandableGroupe mGroupe;
	private String mName;
	private int mType;
	private int mRessource;
	private SearchDestinationListener mListener;

	public ExpandableObject(String name, ExpandableGroupe groupe) {

		mName = name;
		mGroupe = groupe;
		mType = -1;
	}

	public ExpandableObject(String name, ExpandableGroupe groupe, int type) {

		mName = name;
		mGroupe = groupe;
		mType = type;
	}
	
	public ExpandableObject(String name, ExpandableGroupe groupe, int type, int ressource) {

		mName = name;
		mGroupe = groupe;
		mType = type;
		mRessource = ressource;
	}

	public SearchDestinationListener getmListener() {
		return mListener;
	}

	public void setmListener(SearchDestinationListener mListener) {
		this.mListener = mListener;
	}
	
	public int getmType() {
		return mType;
	}

	public void setmType(int mType) {
		this.mType = mType;
	}

	public ExpandableGroupe getmGroupe() {
		return mGroupe;
	}

	public void setmGroupe(ExpandableGroupe mGroupe) {
		this.mGroupe = mGroupe;
	}

	public String getmName() {
		return mName;
	}

	public void setmName(String mName) {
		this.mName = mName;
	}
	
	public int getRessource() {
	
		return mRessource;
	}
}
