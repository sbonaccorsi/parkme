package com.app.ParkMe.Data;

import java.util.ArrayList;

public class ExpandableGroupe {

	private String mName;
	private int mType;
	private ArrayList<ExpandableObject> mArray;

	public ExpandableGroupe(String name, ArrayList<ExpandableObject> array) {

		mName = name;
		mArray = array;
	}

	public ExpandableGroupe(String name) {

		mName = name;
	}
	
	public ExpandableGroupe(String name, int type) {

		mName = name;
		mType = type;
	}

	public String getmName() {
		return mName;
	}

	public void setmName(String mName) {
		this.mName = mName;
	}

	public ArrayList<ExpandableObject> getmArray() {
		return mArray;
	}

	public void setmArray(ArrayList<ExpandableObject> mArray) {
		this.mArray = mArray;
	}
	
	public int getType() {
		
		return mType;
	}
}
