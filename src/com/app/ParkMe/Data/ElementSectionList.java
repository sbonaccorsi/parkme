package com.app.ParkMe.Data;

public class ElementSectionList {

	private int mType;
	private String mName;
	
	public ElementSectionList(int type, String name) {
		
		mType = type;
		mName = name;
	}

	public int getmType() {
		return mType;
	}

	public void setmType(int mType) {
		this.mType = mType;
	}

	public String getmName() {
		return mName;
	}

	public void setmName(String mName) {
		this.mName = mName;
	}
	
}
