package com.app.ParkMe.Data;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferenceManager {

	private static PreferenceManager mInstance;

	private SharedPreferences mSharedPref;
	private Context mContext;

	public static PreferenceManager getInstance(Context c) {

		if (mInstance == null)
			mInstance = new PreferenceManager(c);
		return mInstance;
	}

	public PreferenceManager(Context c) {

		mContext = c;
		mSharedPref = mContext.getSharedPreferences(mContext.getPackageName(), Context.MODE_PRIVATE);
	}

	public void putString(String key, String value) {

		mSharedPref.edit().putString(key, value).commit();
	}

	public void putInt(String key, int value) {

		mSharedPref.edit().putInt(key, value).commit();
	}

	public void putBoolean(String key, boolean value) {

		mSharedPref.edit().putBoolean(key, value).commit();
	}

	public String getString(String key) {

		return mSharedPref.getString(key, null);
	}

	public int getInt(String key) {

		return mSharedPref.getInt(key, -1);
	}

	public boolean getBoolean(String key) {

		return mSharedPref.getBoolean(key, false);
	}
	
	public void clearData() {
		
		mSharedPref.edit().clear().commit();
	}
}
