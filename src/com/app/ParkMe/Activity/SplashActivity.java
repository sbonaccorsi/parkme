package com.app.ParkMe.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.actionbarsherlock.app.SherlockActivity;
import com.app.ParkMe.R;
import com.app.ParkMe.Utils.LogApp;
import com.app.ParkMe.Utils.Utils;

public class SplashActivity extends SherlockActivity {

	private Handler			mHandler;
	private Runnable		mRunnable;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		Utils.setCustomTitleActionBar(getSupportActionBar(), getString(R.string.string_app_name), R.layout.view_custom_title_actb, this);

		mHandler = new Handler();
		mRunnable = new Runnable() {
			
			@Override
			public void run() {
				Intent intent = new Intent(getBaseContext(), SelectVehicleActivity.class);
				startActivity(intent);
			}
		};
		mHandler.postDelayed(mRunnable, 2000);
	}
	
	@Override
	public void onBackPressed() {
		mHandler.removeCallbacks(mRunnable);
		super.onBackPressed();
	}
}
