package com.app.ParkMe.Activity;

import java.net.MalformedURLException;
import java.net.URL;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.provider.Settings;
import android.text.Html;
import android.text.Spanned;

import com.actionbarsherlock.app.SherlockPreferenceActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.app.ParkMe.R;
import com.app.ParkMe.Data.PreferenceManager;
import com.app.ParkMe.Utils.Constant;
import com.app.ParkMe.Utils.Utils;

public class SettingsActivity extends SherlockPreferenceActivity implements Constant, OnPreferenceClickListener {

	private CheckBoxPreference mCheckCameraPref;
	private Preference mLocalisationPref;
	private Preference mDev;
	private Preference mDesign;
	private Preference mShareByMail;
	
	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		Utils.setCustomTitleActionBar(getSupportActionBar(), getString(R.string.string_title_actb_settings),
				R.layout.view_custom_title_actb, this);
		addPreferencesFromResource(R.xml.preferences);
		
		mCheckCameraPref = (CheckBoxPreference) getPreferenceManager().findPreference("preference_camera_animation");
		mLocalisationPref = getPreferenceManager().findPreference("preference_localisation");
		mDev = getPreferenceManager().findPreference("preference_credit_dev");
		mDesign = getPreferenceManager().findPreference("preference_credit_design");
		mShareByMail = getPreferenceManager().findPreference("preference_share_by_mail");
		mDev.setSelectable(false);
		mDesign.setSelectable(false);
		mCheckCameraPref.setOnPreferenceClickListener(this);
		mLocalisationPref.setOnPreferenceClickListener(this);
		mShareByMail.setOnPreferenceClickListener(this);
		
		Boolean value = PreferenceManager.getInstance(getApplicationContext()).getBoolean(KEY_ACTIVE_CAMERA);
		if (value != null)
			mCheckCameraPref.setChecked(value);
		else
			mCheckCameraPref.setChecked(false);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		
		MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.menu_settings, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case android.R.id.home:
			setResult(RESULT_CANCELED);
			finish();
			break;

		default:
			break;
		}
		
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onPreferenceClick(Preference preference) {
		
		if (mCheckCameraPref == preference) {
			if (mCheckCameraPref.isChecked())
				PreferenceManager.getInstance(getApplicationContext()).putBoolean(KEY_ACTIVE_CAMERA, true);
			else
				PreferenceManager.getInstance(getApplicationContext()).putBoolean(KEY_ACTIVE_CAMERA, false);
			return true;
		}
		else if (mLocalisationPref == preference) {
			Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
			startActivity(intent);
			return true;
		}
		else if (mShareByMail == preference) {
			Intent intent = new Intent(Intent.ACTION_SEND);
			intent.setType("text/plain");
			intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.string_share_subject));
			intent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=com.app.ParkMe&feature=search_result#?t=W251bGwsMSwxLDEsImNvbS5hcHAuUGFya01lIl0.");
			startActivity(Intent.createChooser(intent, getString(R.string.string_share_with)));
		}
		return false;
	}
}