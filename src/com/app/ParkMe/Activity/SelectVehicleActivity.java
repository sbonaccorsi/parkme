package com.app.ParkMe.Activity;

import java.util.List;
import java.util.Vector;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.app.ParkMe.R;
import com.app.ParkMe.Adapter.VehicleViewPagerAdapter;
import com.app.ParkMe.Data.PreferenceManager;
import com.app.ParkMe.Database.data.provider.HelpMeToParkMeContent.PARK_TABLE;
import com.app.ParkMe.Database.data.provider.HelpMeToParkMeContent.TOWN_TABLE;
import com.app.ParkMe.Fragment.BusFragment;
import com.app.ParkMe.Fragment.CampingFragment;
import com.app.ParkMe.Fragment.CarFragment;
import com.app.ParkMe.Network.TaskQueue;
import com.app.ParkMe.Utils.Constant;
import com.app.ParkMe.Utils.LogApp;
import com.app.ParkMe.Utils.Utils;
import com.viewpagerindicator.TitlePageIndicator;

public class SelectVehicleActivity extends SherlockFragmentActivity implements Constant, OnPageChangeListener {

	private ViewPager mViewPager;
	private List<Fragment> mListFragment;
	private VehicleViewPagerAdapter mAdapter;
	private TitlePageIndicator mPagerIndicator;
	private ActionBar mActionBar;
	
	private int mCurrentPageIndex = 0;
	
	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		
		setContentView(R.layout.activity_select_vehicle);
		
		PreferenceManager.getInstance(this).putInt("type", -1);
		
		mActionBar = getSupportActionBar();
		Utils.setCustomTitleActionBar(mActionBar, getString(R.string.string_app_name), R.layout.view_custom_title_actb, this);
		
		mViewPager = (ViewPager) findViewById(R.id.viewPager);
		mPagerIndicator = (TitlePageIndicator) findViewById(R.id.indicator);
		Typeface font = Typeface.createFromAsset(getAssets(), "fonts/chunkfive_ex.ttf");
		mPagerIndicator.setTypeface(font);
		
		mListFragment = new Vector<Fragment>();
		mListFragment.add(CarFragment.instantiate(this, CarFragment.class.getName()));
		mListFragment.add(BusFragment.instantiate(this, BusFragment.class.getName()));
		mListFragment.add(CampingFragment.instantiate(this, CampingFragment.class.getName()));
		mAdapter = new VehicleViewPagerAdapter(getSupportFragmentManager(), mListFragment, this);
		mViewPager.setAdapter(mAdapter);
		mViewPager.setCurrentItem(0);
		
		mPagerIndicator.setViewPager(mViewPager);
		mPagerIndicator.setOnPageChangeListener(this);
		
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.menu_select_vehicle, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.menu_settings:
			
			LogApp.e("SelectVehicleActivity :", "ActionBar select settings");
			Intent intentSettings = new Intent(this, SettingsActivity.class);
			startActivityForResult(intentSettings, REQUEST_CODE_ACTIVITY_SETTINGS);
			
			break;
			
		case android.R.id.home :
			finish();
			break;
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	public void startIntentMap() {
		getContentResolver().delete(PARK_TABLE.CONTENT_URI, null, null);
		getContentResolver().delete(TOWN_TABLE.CONTENT_URI, null, null);
		TaskQueue.getInstance().clearQueue();
		
		PreferenceManager.getInstance(this).putInt("type", mCurrentPageIndex);
		Intent intent = new Intent(this, BaseGuiActivity.class);
		intent.putExtra("type", mCurrentPageIndex);
		startActivityForResult(intent, REQUEST_CODE_ACTIVITY_MAP);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		switch (requestCode) {
		case REQUEST_CODE_ACTIVITY_MAP:
			if (resultCode == RESULT_CODE_CHANGE_CATEGORY) {
				mCurrentPageIndex = data.getExtras().getInt("type");
				mViewPager.setCurrentItem(mCurrentPageIndex, true);
				startIntentMap();
			}
			else {
				getContentResolver().delete(PARK_TABLE.CONTENT_URI, null, null);
				getContentResolver().delete(TOWN_TABLE.CONTENT_URI, null, null);
				TaskQueue.getInstance().clearQueue();
			}
			break;
		}
	}
	
	@Override
	public void onPageScrollStateChanged(int arg0) {
	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
	}

	@Override
	public void onPageSelected(int idx) {
		mCurrentPageIndex = idx;
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		mPagerIndicator.setOnPageChangeListener(null);
		for (int idx = 0; idx < mListFragment.size(); ++idx)
			mListFragment.remove(idx);
		mListFragment= null;
		mViewPager = null;
		mPagerIndicator = null;
		mActionBar = null;
	}
}
