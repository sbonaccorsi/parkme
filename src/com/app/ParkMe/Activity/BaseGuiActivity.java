package com.app.ParkMe.Activity;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.Window;
import com.actionbarsherlock.widget.SearchView;
import com.actionbarsherlock.widget.SearchView.OnCloseListener;
import com.actionbarsherlock.widget.SearchView.OnQueryTextListener;
import com.app.ParkMe.R;
import com.app.ParkMe.Data.PreferenceManager;
import com.app.ParkMe.Database.data.provider.HelpMeToParkMeContent.PARK_TABLE;
import com.app.ParkMe.Database.data.provider.HelpMeToParkMeContent.ROAD_TABLE;
import com.app.ParkMe.Database.data.provider.HelpMeToParkMeContent.TOWN_TABLE;
import com.app.ParkMe.Fragment.MapGoogleFragment;
import com.app.ParkMe.Fragment.SearchOptionFragment;
import com.app.ParkMe.Listener.ChangeCategoryListener;
import com.app.ParkMe.Listener.HttpRequestListener;
import com.app.ParkMe.Listener.OnClickMessageBoxButton;
import com.app.ParkMe.Network.AnswerData;
import com.app.ParkMe.Network.TaskManager;
import com.app.ParkMe.Network.TaskQueue;
import com.app.ParkMe.Utils.Constant;
import com.app.ParkMe.Utils.Utils;
import com.google.android.gms.maps.SupportMapFragment;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.LinearLayout;

class DataMenuBaseGuiActivity {

	public static Menu mMenu;
}

public class BaseGuiActivity extends SherlockFragmentActivity implements Constant, HttpRequestListener, 
LocationListener, OnClickMessageBoxButton, ChangeCategoryListener, OnQueryTextListener, OnCloseListener, OnClickListener {

	private MapGoogleFragment mMapFragment;
	private SearchOptionFragment mOptionFragment;

	private SearchView mSearch;
	private String mUrl;
	private PreferenceManager mPref;
	private boolean mIsAlreadySend = false;

	private LocationManager mLocationManager;
	private String mProvider;

	private LinearLayout mContainerNav;
	private ImageButton mArrowUp;
	private ImageButton mArrowDown;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.activity_base_gui_activity);

		getContentResolver().delete(PARK_TABLE.CONTENT_URI, null, null);
		getContentResolver().delete(TOWN_TABLE.CONTENT_URI, null, null);
		TaskQueue.getInstance().clearQueue();

		manageProgressBar(true);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		Utils.setCustomTitleActionBar(getSupportActionBar(), getString(R.string.string_title_actb_map), R.layout.view_custom_title_actb, this);

		mContainerNav = (LinearLayout) findViewById(R.id.container_fleche_nav_dest);
		mArrowUp = (ImageButton) findViewById(R.id.imageButton_up_dest);
		mArrowDown = (ImageButton) findViewById(R.id.imageButton_down_dest);
		mArrowUp.setOnClickListener(this);
		mArrowDown.setOnClickListener(this);

		mPref = PreferenceManager.getInstance(this);
		mMapFragment = (MapGoogleFragment) getSupportFragmentManager().findFragmentByTag(NAME_FRAGMENT_MAP);
		mOptionFragment = (SearchOptionFragment) getSupportFragmentManager().findFragmentByTag(NAME_FRAGMENT_OPTION);

		if (mMapFragment == null) {

			mMapFragment = MapGoogleFragment.newInstance();
			mOptionFragment = (SearchOptionFragment) SherlockFragment.instantiate(this, SearchOptionFragment.class.getName());
			getSupportFragmentManager().beginTransaction().add(R.id.container, mMapFragment, NAME_FRAGMENT_MAP)
			.add(R.id.container, mOptionFragment, NAME_FRAGMENT_OPTION).hide(mOptionFragment).commit();
		}

		mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
		if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
			mProvider = LocationManager.GPS_PROVIDER;
		else
			mProvider = LocationManager.NETWORK_PROVIDER;
	}


	protected void onResume() {
		super.onResume();

		if (!mIsAlreadySend && ((SupportMapFragment)mMapFragment).getMap() != null) {
			Looper loop = null;
			mIsAlreadySend = true;
			mLocationManager.requestSingleUpdate(mProvider, this, loop);
			this.sendRequest();
		}
	}

	@Override
	protected void onPause() {
		mLocationManager.removeUpdates(this);
		super.onPause();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.menu_map_search, menu);
		DataMenuBaseGuiActivity.mMenu = menu;
		mSearch = (SearchView) menu.findItem(R.id.menu_search_destination).getActionView();
		DataMenuBaseGuiActivity.mMenu.findItem(R.id.menu_search_destination).setVisible(false);

		if (mSearch != null) {
			mSearch.setVisibility(View.GONE);
			AutoCompleteTextView searchTextView = (AutoCompleteTextView) mSearch.findViewById(R.id.abs__search_src_text);
			searchTextView.setTextColor(getResources().getColor(R.color.ColorTextBlack));
			mSearch.setOnQueryTextListener(this);
			mSearch.setOnCloseListener(this);
		}
		return super.onCreateOptionsMenu(menu);
	}

	public void handleSearchView() {

		if (DataMenuBaseGuiActivity.mMenu.findItem(R.id.menu_search_destination).isVisible() == false) {
			DataMenuBaseGuiActivity.mMenu.findItem(R.id.menu_search_destination).setVisible(true);
			mSearch.setVisibility(View.VISIBLE);
			mSearch.performClick();
			mSearch.requestFocus();
			mSearch.setIconified(false);
		}
		else {
			DataMenuBaseGuiActivity.mMenu.findItem(R.id.menu_search_destination).setVisible(false);
			mSearch.setVisibility(View.GONE);
		}
	}

	public void displayContainerNav(int val) {
		mContainerNav.setVisibility(val);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.menu_settings:

			Intent intent = new Intent(this, SettingsActivity.class);
			startActivityForResult(intent, REQUEST_CODE_ACTIVITY_SETTINGS);
			break;

		case R.id.menu_switch_icon:

			if (mOptionFragment.isHidden()) {
				getSupportFragmentManager().beginTransaction().show(mOptionFragment).hide(mMapFragment).commit();
				item.setIcon(R.drawable.map_onglet);
				mSearch.setVisibility(View.GONE);
				DataMenuBaseGuiActivity.mMenu.findItem(R.id.menu_search_destination).setVisible(false);
				manageDisplayNavigation(View.GONE);
			}
			else {
				getSupportFragmentManager().beginTransaction().hide(mOptionFragment).show(mMapFragment).commit();
				item.setIcon(R.drawable.search_onglet);
				mSearch.setVisibility(View.GONE);
				DataMenuBaseGuiActivity.mMenu.findItem(R.id.menu_search_destination).setVisible(false);
				manageDisplayNavigation(View.VISIBLE);
			}
			break;

		case android.R.id.home :
			finish();
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	private void manageDisplayNavigation(int visibility) {
		if (mMapFragment.isInSearchDestinationMode()) {
			displayContainerNav(visibility);
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putBoolean("SendRequest", true);
		outState.putBoolean("RequestDone", true);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		mIsAlreadySend = savedInstanceState.getBoolean("SendRequest");
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent data) {
		super.onActivityResult(requestCode, resultCode, data);		

		switch (requestCode) {
		case REQUEST_CODE_ACTIVITY_PARK:
			mIsAlreadySend = true;
			break;

		case REQUEST_CODE_ACTIVITY_SETTINGS:
			mIsAlreadySend = true;
			break;

		default:
			break;
		}
	}

	public void launchIntentParkActivity(String title, double latDest, double lonDest, double latMy, double lonMy) {
		Intent intent = new Intent(this, ParkActivity.class);
		intent.putExtra("namePark", title);
		intent.putExtra("latDest", latDest);
		intent.putExtra("lonDest", lonDest);
		intent.putExtra("latMy", latMy);
		intent.putExtra("lonMy", lonMy);

		getContentResolver().delete(ROAD_TABLE.CONTENT_URI, null, null);

		startActivityForResult(intent, REQUEST_CODE_ACTIVITY_PARK);
	}

	public void sendRequest() {

		this.initUrlByType();
		manageProgressBar(true);
		TaskManager task = new TaskManager(this, this, mUrl, REQUEST_TYPE_BASE, null);
		TaskQueue.getInstance().addTaskInQueue(task);
	}

	public void searchAroundLocation(int idSelection, String dest) {

		if (idSelection == 0) {

			if (mMapFragment != null) {
				mMapFragment.clearMap();
				mMapFragment.centerMapOnUser();
				mMapFragment.startRequestDistance(null);
			}
		}
		else if (idSelection == 1) {
			if (mMapFragment != null) {
				mMapFragment.searchDestination(dest);
			}
		}
		if (mOptionFragment.isHidden()) {
			getSupportFragmentManager().beginTransaction().hide(mMapFragment).show(mOptionFragment).commit();
			DataMenuBaseGuiActivity.mMenu.findItem(R.id.menu_switch_icon).setIcon(R.drawable.map_onglet);
			mSearch.setVisibility(View.GONE);
			DataMenuBaseGuiActivity.mMenu.findItem(R.id.menu_search_destination).setVisible(false);
			manageDisplayNavigation(View.GONE);
		}
		else {
			getSupportFragmentManager().beginTransaction().hide(mOptionFragment).show(mMapFragment).commit();
			DataMenuBaseGuiActivity.mMenu.findItem(R.id.menu_switch_icon).setIcon(R.drawable.search_onglet);
			mSearch.setVisibility(View.GONE);
			DataMenuBaseGuiActivity.mMenu.findItem(R.id.menu_search_destination).setVisible(false);
			manageDisplayNavigation(View.VISIBLE);
		}
	}

	private void initUrlByType() {

		if (getIntent().getExtras().getInt("type") == 0) {
			mPref.putInt("flagValue", R.drawable.icon_voiture);
			mUrl = URL_BASE_CAR;
		}
		else if (getIntent().getExtras().getInt("type") == 1) {
			mPref.putInt("flagValue", R.drawable.icon_bus);
			mUrl = URL_BASE_BUS;
		}
		else if (getIntent().getExtras().getInt("type") == 2) {
			mPref.putInt("flagValue", R.drawable.icon_camping);
			mUrl = URL_BASE_CAMPING;
		}
	}

	public void manageProgressBar(boolean value) {
		setSupportProgressBarIndeterminateVisibility(value);
	}

	@Override
	public void onRequestIsComplete(final AnswerData answer) {

		manageProgressBar(false);

		switch (answer.getTypeRequest()) {
		case REQUEST_TYPE_BASE:
			if (answer.getStatusRequest() == BAD_ANSWER) {
				if (answer.getStatusCode() == REQUEST_RESPONSE_NO_NETWORK) {
					Crouton.makeText(BaseGuiActivity.this, getString(R.string.string_error_connection), Style.ALERT).show();
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onLocationChanged(Location location) {
		if (location != null)
			mMapFragment.centerMapOnLocation(location);
	}

	@Override
	public void onProviderDisabled(String provider) {
	}

	@Override
	public void onProviderEnabled(String provider) {
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
	}

	@Override
	public void OnClickPositiveButton(DialogInterface dialog, int which) {
		dialog.cancel();
	}

	@Override
	public void OnClickNegativeButton(DialogInterface dialog, int which) {
		dialog.cancel();
	}

	@Override
	public void OnClickNeutralButton(DialogInterface dialog, int which) {
	}


	@Override
	public void changeCategory(int childPosition) {

		if (TaskQueue.getInstance().getCurrentTask() != null) {
			TaskQueue.getInstance().cancelAllTask(true);
			TaskQueue.getInstance().clearQueue();
		}
		Crouton.clearCroutonsForActivity(this);

		PreferenceManager.getInstance(this).putInt("type", childPosition);
		getContentResolver().delete(PARK_TABLE.CONTENT_URI, null, null);
		getContentResolver().delete(TOWN_TABLE.CONTENT_URI, null, null);

		getIntent().putExtra("type", childPosition);
		setResult(RESULT_CODE_CHANGE_CATEGORY, getIntent());
		finish();
	}


	@Override
	public boolean onQueryTextSubmit(String query) {
		this.searchAroundLocation(1, query);
		return true;
	}


	@Override
	public boolean onQueryTextChange(String newText) {
		return false;
	}


	@Override
	public boolean onClose() {
		mSearch.setVisibility(View.GONE);
		DataMenuBaseGuiActivity.mMenu.findItem(R.id.menu_search_destination).setVisible(false);
		return true;
	}

	@Override
	public void onBackPressed() {

		if (mOptionFragment.isHidden() == false) {
			getSupportFragmentManager().beginTransaction().hide(mOptionFragment).show(mMapFragment).commit();
			DataMenuBaseGuiActivity.mMenu.findItem(R.id.menu_switch_icon).setIcon(R.drawable.search_onglet);
			mSearch.setVisibility(View.GONE);
			DataMenuBaseGuiActivity.mMenu.findItem(R.id.menu_search_destination).setVisible(false);
			manageDisplayNavigation(View.VISIBLE);
		}
		else {
			boolean ret = true;
			if (mMapFragment != null)
				ret = mMapFragment.clearMap();
			if (ret == false) {
				if (TaskQueue.getInstance().getCurrentTask() != null) {
					TaskQueue.getInstance().cancelAllTask(true);
					TaskQueue.getInstance().clearQueue();
				}
				Crouton.clearCroutonsForActivity(this);
				finish();
			}
		}
	}

	@Override
	public void onClick(View v) {

		if (mArrowDown == v) {
			mMapFragment.prevDestination();
		}
		else if (mArrowUp == v) {
			mMapFragment.nextDestination();
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		mArrowUp.setOnClickListener(null);
		mArrowDown.setOnClickListener(null);
		mSearch.setOnQueryTextListener(null);
		mSearch.setOnCloseListener(null);
		mMapFragment = null;
		mOptionFragment = null;
		mSearch = null;
		mUrl = null;
		mPref = null;
		mLocationManager = null;
		mProvider = null;
		mContainerNav = null;
		mArrowDown = null;
		mArrowUp = null;
	}
}
