package com.app.ParkMe.Activity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.app.ParkMe.R;
import com.app.ParkMe.Fragment.ParkDescriptionFragment;
import com.app.ParkMe.Fragment.ParkRoadFragment;
import com.app.ParkMe.Listener.HttpRequestListener;
import com.app.ParkMe.Map.Road;
import com.app.ParkMe.Map.RoadProvider;
import com.app.ParkMe.Network.AnswerData;
import com.app.ParkMe.Network.ExecuteAsyncTask;
import com.app.ParkMe.Network.TaskManager;
import com.app.ParkMe.Utils.Constant;
import com.app.ParkMe.Utils.LogApp;
import com.app.ParkMe.Utils.Utils;
import com.google.android.gms.maps.model.LatLng;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

class DataMenuParkActivity {

	public static Menu mMenu;
}

public class ParkActivity extends SherlockFragmentActivity implements Constant, HttpRequestListener {

	private ParkDescriptionFragment mFragParkDesc;
	private ParkRoadFragment mFragParkRoad;
	private Road mRoad;

	private LatLng mCoordDest;
	private LatLng mCoordMy;

	private boolean mIsAlreadySend = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_park);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		Utils.setCustomTitleActionBar(getSupportActionBar(), getString(R.string.string_title_actb_description), 
				R.layout.view_custom_title_actb, this);

		mCoordDest = new LatLng(getIntent().getExtras().getDouble("latDest"), 
				getIntent().getExtras().getDouble("lonDest"));

		if (getIntent().getExtras().getDouble("latMy") != -1 && getIntent().getExtras().getDouble("lonMy") != -1)
			mCoordMy = new LatLng(getIntent().getExtras().getDouble("latMy"), 
					getIntent().getExtras().getDouble("lonMy"));

		//				mCoordDest = new LatLng(43.29905582663548, 
		//						5.382829383015633);
		//		
		//				mCoordMy = new LatLng(43.2991967, 
		//						5.3852511);

		mFragParkDesc = (ParkDescriptionFragment) getSupportFragmentManager().findFragmentByTag(NAME_FRAGMENT_PARK_DESC);
		mFragParkRoad = (ParkRoadFragment) getSupportFragmentManager().findFragmentByTag(NAME_FRAGMENT_PARK_ROAD);
		if (mFragParkDesc == null && mFragParkRoad == null) {

			mFragParkDesc = (ParkDescriptionFragment) SherlockFragment.instantiate(this, ParkDescriptionFragment.class.getName());
			mFragParkRoad = (ParkRoadFragment) SherlockFragment.instantiate(this, ParkRoadFragment.class.getName());

			getSupportFragmentManager().beginTransaction()
			.add(R.id.container_frag_park, mFragParkDesc, NAME_FRAGMENT_PARK_DESC)
			.add(R.id.container_frag_park, mFragParkRoad, NAME_FRAGMENT_PARK_ROAD)
			.hide(mFragParkRoad).commit();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();

		if (!mIsAlreadySend) {
			if (mCoordMy != null) {
				String url = RoadProvider.getUrlRoadMap(mCoordMy.latitude, mCoordMy.longitude, mCoordDest.latitude, mCoordDest.longitude);
				TaskManager task = new TaskManager(this, this, url, REQUEST_TYPE_ROAD, null);
				ExecuteAsyncTask async = new ExecuteAsyncTask();
				async.execute(task);
				mIsAlreadySend = true;
			}
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putBoolean("SendRequest", true);
		outState.putBoolean("DescVisible", mFragParkDesc.isHidden());
		outState.putBoolean("RoadVisible", mFragParkRoad.isHidden());
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		mIsAlreadySend = savedInstanceState.getBoolean("SendRequest");
		if (savedInstanceState.getBoolean("DescVisible")) {
			getSupportFragmentManager().beginTransaction().show(mFragParkRoad).hide(mFragParkDesc).commit();
			Utils.setTextCustomTitleActionBar(getSupportActionBar(), getString(R.string.string_title_actb_route));
			MenuItem item = DataMenuParkActivity.mMenu.findItem(R.id.menu_park_switch);
			item.setIcon(R.drawable.action_about);
		}
		if (savedInstanceState.getBoolean("RoadVisible")) {
			getSupportFragmentManager().beginTransaction().show(mFragParkDesc).hide(mFragParkRoad).commit();
			Utils.setTextCustomTitleActionBar(getSupportActionBar(), getString(R.string.string_title_actb_description));
			MenuItem item = DataMenuParkActivity.mMenu.findItem(R.id.menu_park_switch);
			item.setIcon(R.drawable.collections_view_as_list);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.menu_park, menu);
		DataMenuParkActivity.mMenu = menu;
		if (mFragParkDesc.isHidden())
			DataMenuParkActivity.mMenu.findItem(R.id.menu_park_switch).setIcon(R.drawable.action_about);
		else
			DataMenuParkActivity.mMenu.findItem(R.id.menu_park_switch).setIcon(R.drawable.collections_view_as_list);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.menu_park_switch:

			if (!mFragParkDesc.isHidden()) {
				getSupportFragmentManager().beginTransaction().show(mFragParkRoad).hide(mFragParkDesc).commit();
				Utils.setTextCustomTitleActionBar(getSupportActionBar(), getString(R.string.string_title_actb_route));
				item.setIcon(R.drawable.action_about);
			}
			if (!mFragParkRoad.isHidden()) {
				getSupportFragmentManager().beginTransaction().show(mFragParkDesc).hide(mFragParkRoad).commit();
				Utils.setTextCustomTitleActionBar(getSupportActionBar(), getString(R.string.string_title_actb_description));
				item.setIcon(R.drawable.collections_view_as_list);
			}
			break;

		case R.id.menu_settings:

			LogApp.e("ParkActivity :", "ActionBar select settings");
			Intent intent = new Intent(this, SettingsActivity.class);
			startActivityForResult(intent, REQUEST_CODE_ACTIVITY_SETTINGS);
			break;

		case android.R.id.home:
			setResult(RESULT_CANCELED);
			finish();
			break;

		case R.id.menu_gps:
			this.startIntentGPS(mCoordDest);
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		switch (requestCode) {
		case REQUEST_CODE_ACTIVITY_NAVIGATOR:
			mIsAlreadySend = true;
			break;

		case REQUEST_CODE_ACTIVITY_SETTINGS:
			mIsAlreadySend = true;
			break;

		default:
			break;
		}
	}

	public void startIntentGPS(LatLng coord) {

		try {
			Intent i = new Intent(Intent.ACTION_VIEW, 
					Uri.parse("google.navigation:q=" + coord.latitude +
							"," + coord.longitude)); 
			startActivityForResult(i, REQUEST_CODE_ACTIVITY_NAVIGATOR);
		} catch (ActivityNotFoundException e) {
			Toast.makeText(this, "GPS Navigator can't be launch", Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void onRequestIsComplete(final AnswerData answer) {

		switch (answer.getTypeRequest()) {
		case REQUEST_TYPE_ROAD :

			if (answer.getStatusRequest() == BAD_ANSWER) {
				if (answer.getStatusCode() == REQUEST_RESPONSE_NO_NETWORK)
					Crouton.makeText(ParkActivity.this, getString(R.string.string_error_connection), Style.ALERT).show();
			}
			break;

		default :
			break;
		}
	}
}
